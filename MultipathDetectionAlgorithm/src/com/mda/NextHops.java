package com.mda;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import android.util.Log;
import android.util.SparseArray;

import com.mda.types.Interface;
import com.mda.types.Interface.ProbeContext;
import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

public class NextHops extends Thread {

	private static final String TAG = "NextHops";
	private static final int MAX_INTERFACES = 16;
	private static final int[] probesToSendTable = 
	//	 0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
		{1 , 6, 11, 16, 21, 27, 33, 38, 44, 51, 57, 63, 70, 76, 85, 90, 96 };
	
	private Interface r;
	private int h;
	protected SparseArray<Set<Interface>> InterfaceSetForEachHop;
	
	/**
	 * This ProbeParameters object is maintained in order
	 * to pass it to the Interface.getFlowPort() method.
	 * In this way the sent probe has the same fields as
	 * the algorithm probes and can be associated with the
	 * same flows.
	 * 
	 * @see selectFlow()
	 * @see Interface.getFlowPort()
	 */
	private ProbeContext context;
	
	public NextHops(SparseArray<Set<Interface>> isfeh, Interface r, 
			int h, InetAddress destIP, int destPort, int timeout, int attempts) {
		
		InterfaceSetForEachHop = isfeh;
		this.r = r;
		this.h = h;

		// Constructs the ProbeParameters object to be passed to
		// Interface.getFlowPort().
		context = new ProbeContext();
		
		context.params = new ProbeParameters(
			destIP.getHostAddress(),
			0, // sourcePort, getFlowPort() will change it
			destPort,
			1, // ttl, getFlowPort() will change it
			timeout,
			attempts
		);
		
	}

	public Interface getInterface() {
		return r;
	}
	
	public Set<Interface> getSuccessors() {
		return r.getSuccessorsSet();
	}
	
	/**
	 * NextHops function in MDA algorithm paper.
	 * @param r the interface of which I want to know its successors
	 * @param h the hop number
	 * @param interfaceSetForEachHop the array that contains all the interfaces for each hop
	 */
	@Override
	public void run() {
		
		int flowPort, sent = 0, received = 0, n = 1, max;
		boolean flowNotFound = false;
		ArrayList<Prober> probers = new ArrayList<Prober>();
		ArrayList<Integer> flowPorts = new ArrayList<Integer>();
		Prober p;
		
doloop:	do {
			n = r.getSuccessorsSet().size();
			if (n == 0) n = 1;

			max = probesToSend(n);
			
			// Send probes in parallel
			
			for (; sent < max; sent++) {
				
				flowPort = selectFlow(sent, h-1, r);
				if (flowPort == -1) {
					flowNotFound = true;
					Log.w(TAG, "NO FLOW, exiting prematurely...");
					break;
				}
					
				flowPorts.add(sent, flowPort);
				p = sendProbe(h, flowPort);
				probers.add(sent, p);
			}
			
			for (; received < sent; received++) {
				p = probers.get(received);
				try {
					// Wait for probes termination
					p.join();
				} catch (InterruptedException e) {
					Log.e(TAG, "Prober interrupted.");
					e.printStackTrace();
					return;
				}
				
				if (p.getResult() == ProbeResult.NO_ERROR ||
						p.getResult() == ProbeResult.DEST_REACHED) {
					
					Interface s = p.getInterface();
					Log.d(TAG, this.getId() + " " + String.format("%02d", h) + ": " + String.format("%05d", flowPorts.get(received))+ " => " + s.getStringAddress());
					
					// add the new interface, if exists, returns the old instance
					Interface resolvedSuccessor = resolveSuccessor(r.addSuccessor(s));
					
					synchronized (InterfaceSetForEachHop) {
						resolvedSuccessor.setFlowPort(h, flowPorts.get(received));
					}
					
					if (r.getSuccessorsSet().size() > n) {
						// if I've discovered a new interface I can send a new
						// bunch of probes without wait for the termination of
						// the already started ones
						received++;
						continue doloop;
					}
				}
				else {
					Log.e(TAG, this.getId() + " " + String.format("%02d", h) + ": " + String.format("%05d", flowPorts.get(received))+ " => " + ProbeResult.getCodeString(p.getResult()));
				}
			}
			
			if (flowNotFound)
				break;

		} while (r.getSuccessorsSet().size() > n);
		
		if (r.getSuccessorsSet().size() > 1 && !r.isUnresponsiveHop()) {
			if (perPacket(r, h))
				r.setLoadBalancer(Interface.BalanceType.PER_PACKET_BAL);
			else
				r.setLoadBalancer(Interface.BalanceType.PER_FLOW_BAL);
		}
	}
	
	// returns already present Interface instance if exists, avoids port assignment partition
	private Interface resolveSuccessor(Interface nextHopSuccessor) {
		synchronized(InterfaceSetForEachHop) {
			Set<Interface> hopInterfaceSet = InterfaceSetForEachHop.get(h);
			if (hopInterfaceSet.contains(nextHopSuccessor)) {
				for (Interface i : hopInterfaceSet) {
					if (i.compareTo(nextHopSuccessor) == 0)
						return i;
				}
			}
			
			hopInterfaceSet.add(nextHopSuccessor);
			return nextHopSuccessor;
			
		}
	}

	/**
	 * SelectFlow of the MDA algorithm paper.
	 * @param k randomizer integer
	 * @param h	hop number
	 * @param r	interface
	 * @return a port to reach the interface @p r at hop @p h, different values 
	 * 	of k give different results
	 */
	private int selectFlow(int k, int h, Interface r) {

		// If r is an unresponsive hop we choose the first port of the first
		// responsive hop which is an antecedent r.
		
		if (r.isUnresponsiveHop()) {
			try {
				// Find the first antecedent that is not unresponsive
				// The object "InterfaceSetForEachHop" is shared
				// between all the threads, we have to act on it in
				// mutual exclusion.
				
				// the smallest key (hop number) after the localhop in the hop set
				// should be:
				// keyAt(0) == hMin-1
				// keyAt(1) == hMin
				final int hMin = InterfaceSetForEachHop.keyAt(1);
				
				synchronized(InterfaceSetForEachHop) {
					// i = hop number
					for (int i = h-1; i >= hMin; i--) {
						// check whether there are responsive hops
						// within all interfaces of an hop ..
						Set<Interface> interfaceSet = InterfaceSetForEachHop.get(i);
						Iterator<Interface> interfacesOfAnHop = interfaceSet.iterator();
						while (interfacesOfAnHop.hasNext()) {
							Interface aHop = interfacesOfAnHop.next();
							if (!aHop.isUnresponsiveHop()) {
								context.interfacesInHop = interfaceSet.size();
								int flow = aHop.getFlowPort(i, k, context);
								if (flow == -1)
									continue;
								return flow;
							}
						}
						// ..or continue searching backward
					}
				}
				
			} catch (Exception e) {
				Log.e(TAG, "Failed to get previous h flow port for an unresponsive h");
				e.printStackTrace();
				return -1;
			}
			
		}
		
		synchronized (InterfaceSetForEachHop) {
			context.interfacesInHop = InterfaceSetForEachHop.get(h).size();
		}
		
		int flow = r.getFlowPort(h, k, context);
		Log.v(TAG, "Select flow for interface (tid.ip.k)" + this.getId() + "."
				+ r.getStringAddress() + "." + k + " at hop " + h
				+ ", MIH size: " + context.interfacesInHop + " => " + flow);
		return flow;
	}
	
	/**
	 * PerPacket function in MDA algorithm paper.
	 * @param r the interface of the load balancer i want to categorize
	 * @param h the h number
	 * @return true if it's a per-packet load balancer, false otherwise
	 */
	public boolean perPacket(Interface r, int h) {
		
		/**
		 * Initialization of flow port and the set of next-hop interface
		 * to which our probes are forwarded.
		 */
		final int PER_PACKET_PROBES = probesToSend(1);
		int flowPort = selectFlow(0, h-1, r);
		Interface r1 = new Interface(r.getInetAddress()); 		
		
		for (int i = 0; i < PER_PACKET_PROBES; i++) {

			Prober p = sendProbe(h, flowPort);
			
			try {
				p.join();
			} catch (InterruptedException e) {
				Log.e(TAG, "Probe interrupted during perPacket.");
				e.printStackTrace();
				continue;
			}
			
			Log.v(TAG, "perP " + String.format("%02d", h) + ": " + r.getStringAddress() + ":" + String.format("%05d", flowPort) + " => " + p.getInterface().getStringAddress());
			
			if (p.getResult() == ProbeResult.NO_ERROR)
				r1.addSuccessor(p.getInterface());

			/**
			 * If the probes are forwarded to a different interface, then
			 * the load balancer performs per-packet balancing and i can stop
			 * here
			 */	
			if (r1.getSuccessorsSet().size() > 1) {
				Log.v(TAG,"PerPacket balancer found!");
				for (Interface in: r1.getSuccessorsSet())
					Log.v(TAG, in.getStringAddress());
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * SendProbe function.
	 * It uses a pool of pre-created thread to perform sending probes.
	 * @param ttl = ttl
	 * @param flowPort = port
	 */
	private Prober sendProbe(int ttl, int flowPort) {
		Prober p;
		ProbeParameters par = new ProbeParameters(
			context.params.getDestIp(), 
			flowPort,
			context.params.getDestPort(), 
			ttl, 
			context.params.getTimeout(),
			context.params.getAttempts());
			
		p = new Prober(par);
		p.start();

		return p;
	}
	
	/**
	 * ProbesToSend function of the MDA algorithm paper.
	 * @param n estimated number of interfaces
	 * @return number of probes to send to have a 95% confidence that the
	 * estimated number of interfaces is correct
	 */
	public static int probesToSend(int n) {
		if (n <  0 || n > MAX_INTERFACES)
			return -1;
		else
			return probesToSendTable[n];
	}
	
}
