package com.mda;

import java.lang.ref.WeakReference;
import java.util.Set;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mda.types.Interface;

public class GraphViewActivity extends Activity {
	
	protected static final String TAG = "GraphViewActivity";
	String ipAddress, shareText, shareTextLB;
	int currentHop, destPort, initialTTL, maxTTL, maxResp, maxAttempts, maxUnresponsiveHops;
	Boolean showPortNumber, showAllFlows;
	
	private static class MyHandler extends Handler {
		private static final String TAG = "GraphViewActivity";

		private final WeakReference<GraphViewActivity> wActivity;

		public MyHandler(GraphViewActivity activity) {
			wActivity = new WeakReference<GraphViewActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			GraphViewActivity activity = wActivity.get();
			if (activity == null || msg == null)
				return;
			
			switch(msg.what) {
			case MultipathTraceroute.MSG_START:
				activity.graphWebView.loadUrl("javascript:initGraph()");
				break;
			
			case MultipathTraceroute.MSG_INTERFACE:
				if (msg.obj == null)
					return;

				Interface i = (Interface) msg.obj;
				
				activity.graphWebView.loadUrl("javascript:addNode('" + i.getStringAddress()  + "')");
				
				Set<Interface> successors = i.getSuccessorsSet();
				if (successors.size() > 0) {
					if (successors.size() > 1)
						activity.graphWebView.loadUrl("javascript:isLoadBalancer('" + i.getStringAddress()  + "')");
				
					for (Interface s: successors) {
						activity.graphWebView.loadUrl("javascript:addNode('" + s.getStringAddress()  + "')");
						activity.graphWebView.loadUrl("javascript:addEdge('" + i.getStringAddress()  + "', '" + s.getStringAddress() + "')");
					}
				}
				else {
					activity.graphWebView.loadUrl("javascript:addUnresponsive()");
					activity.graphWebView.loadUrl("javascript:addEdge('" + i.getStringAddress()  + "', '*')");
				}
				break;
				
			case MultipathTraceroute.MSG_HOP:
				activity.graphWebView.loadUrl("javascript:newHop()");
				break;
			
			case MultipathTraceroute.MSG_END:
				activity.graphWebView.loadUrl("javascript:finish()");
				break;

			default:
				Log.e(TAG, "Unexpected Message");
			}
		}
	};

	public final MyHandler resultsHandler = new MyHandler(this);
	private WebView graphWebView;
	private MultipathTraceroute tracer;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null)
			return;
		else
			ipAddress = extras
				.getString(getString(R.string.key_destination_ip));

		loadPreferences();
		
		graphWebView = new WebView(this);
		setContentView(graphWebView);
		
		graphWebView.getSettings().setJavaScriptEnabled(true);
		
		graphWebView.setWebChromeClient(new WebChromeClient() {
			  public void onConsoleMessage(String message, int lineNumber, String sourceID) {
			    Log.i(TAG, message + " -- From line "
			                         + lineNumber + " of "
			                         + sourceID);
			  }
			});
		
		graphWebView.setWebViewClient(new WebViewClient() {
			@Override
		    public void onPageFinished(WebView view, String url) {
		        //view.loadUrl("javascript:initGraph()");
		        tracer.start();
				//view.loadUrl("javascript:addNode('asd')");
				//view.loadUrl("javascript:addNode('asdasf')");
				//view.loadUrl("javascript:addEdge('asd', 'asdasf')");
		    }
		});
		
		tracer = new MultipathTraceroute(ipAddress, resultsHandler, destPort,
				(short) initialTTL, (short) maxTTL, maxResp, maxAttempts, maxUnresponsiveHops);
		
		graphWebView.loadUrl("file:///android_asset/graph.html");
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.graph_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void loadPreferences() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		destPort = Integer.valueOf(settings.getString(
				getString(R.string.prefs_destination_port),
				getString(R.string.mda_dest_port_default)));

		initialTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_initial_ttl),
				getString(R.string.mda_initial_ttl_default)));

		maxTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_ttl),
				getString(R.string.mda_max_ttl_default)));

		maxResp = Integer.valueOf(settings.getString(
				getString(R.string.prefs_timeout),
				getString(R.string.mda_max_resp_wait_default)));

		maxAttempts = Integer.valueOf(settings.getString(
				getString(R.string.prefs_attempts),
				getString(R.string.mda_max_attempts_default)));
		
		maxUnresponsiveHops = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_unresponsive_hops),
				getString(R.string.mda_max_unresponsive_hops_default)));

		showPortNumber = Boolean.valueOf(settings.getBoolean(getString(R.string.prefs_show_port_number),
				Boolean.valueOf(getString(R.string.mda_show_port_number_default))));
		
		showAllFlows = Boolean.valueOf(settings.getBoolean(getString(R.string.prefs_show_all_flows),
				Boolean.valueOf(getString(R.string.mda_show_all_flows_default))));

	}

}
