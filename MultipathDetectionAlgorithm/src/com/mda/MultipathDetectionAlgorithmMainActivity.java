package com.mda;

import java.lang.ref.WeakReference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author andrea
 * 
 */
public class MultipathDetectionAlgorithmMainActivity extends ActionBarActivity implements OnClickListener {
	
	private static final String TAG = "MDAActivity";

	private Context context;
	private Class<?> cls;
	
	String ipAddress;
	int sourcePort, destPort, initialTTL, maxTTL, maxResp, maxAttempts;
	Button buttonMultipathTraceroute, buttonTraceroute, buttonAdvancedSettings, buttonGraphMDA;
	EditText ipAddrField;
	ProgressBar progress;

	private static class MyHandler extends Handler {
		private final WeakReference<MultipathDetectionAlgorithmMainActivity> wActivity;

		public MyHandler(MultipathDetectionAlgorithmMainActivity activity) {
			wActivity = new WeakReference<MultipathDetectionAlgorithmMainActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			MultipathDetectionAlgorithmMainActivity activity = wActivity.get();
			if (activity == null || msg == null)
				return;

			switch (msg.what) {
				case DNSRequest.DSN_LOOKUP_OK:
					if (msg.obj == null)
						return;

					activity.ipAddrField.setText((String)msg.obj);
					activity.createIntent(activity.context, activity.cls);
					break;
				
				case DNSRequest.DNS_LOOKUP_FAILED:
					activity.ipAddrField.setError(activity.getString(R.string.mda_error_ip));
					break;
					
				case DNSRequest.DNS_LOOKUP_TIMEOUT:
					activity.ipAddrField.setError(activity.getString(R.string.mda_error_ip_timeout));
					break;

				default:
					Log.d(TAG, "Unexpected Message");
			}

			activity.enableInput(true);
		}
	};

	public final MyHandler dnsHandler = new MyHandler(this);

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multipath_detection_algorithm_main);
		
		buttonMultipathTraceroute = (Button) findViewById(R.id.button_multipath_traceroute);
		buttonTraceroute = (Button) findViewById(R.id.button_traceroute);
		buttonGraphMDA = (Button) findViewById(R.id.button_graph_mda);
		
		ipAddrField = (EditText) findViewById(R.id.ip_addr_field);
		progress = (ProgressBar) findViewById(R.id.progress_bar_main);
		
		String lastIpUsed = PreferenceManager.getDefaultSharedPreferences(this)
				.getString(getString(R.string.prefs_last_dest_ip_used), "");
		
		ipAddrField.setText(lastIpUsed);
	
		buttonMultipathTraceroute.setOnClickListener(this);
		buttonTraceroute.setOnClickListener(this);
		buttonGraphMDA.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.multipath_detection_algorithm,
				menu);
		return true;
	}

	/**
	 * Event Handling for Individual menu item selected Identify single menu
	 * item by it's id
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.menu_item_info:
			((TextView) (new AlertDialog.Builder(this))
				.setTitle(R.string.menu_item_info)
				.setMessage(R.string.info_text)
				.show()
				.findViewById(android.R.id.message))
				.setMovementMethod(LinkMovementMethod.getInstance());
			return true;
			
			case R.id.menu_item_preferences:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * onClick function for buttons on the main activity
	 */
	public void onClick(View v) {
		if (!checkConnection())
			return;

		Class<?> c;
		if (v == buttonMultipathTraceroute)
			c = MultipathTracerouteActivity.class;
		else if (v == buttonTraceroute)
			c = TracerouteActivity.class;
		else
			c = GraphViewActivity.class;
		
		if(setIpAddress()) // no DNS request
			createIntent(v.getContext(), c);
		else { // DNS request
			context = v.getContext();
			cls = c;
		}
	}
	
	/** Common method to create an Intent.
	 * @param context	A Context of the application package 
	 * 					implementing this class
	 * @param cls 		The component class that is to be used for 
	 * 					the intent;
	 */
	private void createIntent(Context context, Class<?> cls) {
		if (!checkConnection() || !setIpAddress())
			return;
		
		Intent intent = new Intent(context, cls);
		intent.putExtra(getString(R.string.key_destination_ip), ipAddress);
		
		// save last IP used
		PreferenceManager.getDefaultSharedPreferences(this)
		.edit()
		.putString(getString(R.string.prefs_last_dest_ip_used), ipAddress)
		.commit();
		
		startActivity(intent);
	}

	/**
	 * Check the network Connection.
	 * 
	 * @return true if a network connection is available, false otherwise
	 */
	private boolean checkConnection() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("No network connection available.");
			AlertDialog dialog = builder.create();
			dialog.show();
			return false;
		}
	}

	/**
	 * Reads the ip address fields and validate it.
	 */
	private boolean setIpAddress() {
		ipAddress = ipAddrField.getText().toString().trim();
		
		if (Patterns.IP_ADDRESS.matcher(ipAddress).matches())
			// The address is in dotted decimal notation
			return true;

		// The address is symbolic
		(new DNSRequest(dnsHandler)).execute(ipAddress);

		enableInput(false);
		
		return false;
	}
	
	private void enableInput(boolean enable) {
		progress.setVisibility(enable ? View.INVISIBLE : View.VISIBLE);
		ipAddrField.setEnabled(enable);
		buttonTraceroute.setEnabled(enable);
		buttonMultipathTraceroute.setEnabled(enable);
	}
	
}
