/**
 * This file contains the class definition of a Java
 * implementation of the MDA algorithm, as described on the
 * 'Multipath Detection Algorithm for Paris Traceroute' paper.
 */
package com.mda;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;

import com.mda.types.Interface;

/**
 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
 *
 */
public class MultipathTraceroute extends Thread {

	private static final String	TAG					= "MDATR_ALGO";

	public static final short	DEFAULT_HMIN		= 1;
	public static final short	DEFAULT_HMAX		= 37;
	public static final int		DEFAULT_DEST_PORT	= 80;
	public static final int		DEFAULT_TIMEOUT_MS	= 5000;
	public static final int		DEFAULT_ATTEMPTS	= 1;

	public static final int		MSG_START			= 1;
	public static final int		MSG_HOP				= 2;
	public static final int		MSG_HOP_IS_MIH		= 3;
	public static final int		MSG_INTERFACE		= 4;
	public static final int		MSG_DEST_REACHED	= 5;
	public static final int		MSG_END				= 6;
	public static final int 	MSG_PAUSED			= 7;
	public static final int 	MSG_RESUMED			= 8;

	protected InetAddress destinationIP = null;
	protected int destinationPort;
	protected short hMax;
	protected short hMin;
	protected int timeout;
	protected int attempts;
	protected int maxUnresponsiveHops;

	/**
	 * A list containing for each element (hop) a list of the reachable interfaces
	 * (the list of MIHs, using paper terminology).
	 *
	 * E.g.: the first element contains a list of interfaces reachable at hop 1,
	 * the second contains the list of interfaces reachable at hop 2,
	 * and so on..
	 */
	protected SparseArray<Set<Interface>> hopInterfaceSet = new SparseArray<Set<Interface>>();
	private Handler handler;

	private boolean paused = false;
	private Object pauseLock = new Object();

	/**
	 * Constructs a MultipathTraceroute instance.
	 * @param address dotted-decimal ip string or hostname for the destination address
	 * @param h	Handler instance to which send messages containing algorithm outputs
	 */

	public MultipathTraceroute(String address, Handler h) {
		setDestinationAddress(address);
		this.destinationPort = DEFAULT_DEST_PORT;
		this.hMin = DEFAULT_HMIN;
		this.hMax = DEFAULT_HMAX;
		this.timeout = DEFAULT_TIMEOUT_MS;
		this.attempts = DEFAULT_ATTEMPTS;
		this.handler = h;
	}

	/**
	 * Constructs a MultipathTraceroute instance.
	 * @param address dotted-decimal ip string or hostname representing the destination address
	 * @param h	Handler instance to which send messages containing algorithm outputs
	 * @param destPort the destination port of the target
	 * @param hMin the minimum hop number from which the traceroute will start
	 * @param hMax the maximum hop number reachable by the algorithm
	 * @param timeout maximum time in milliseconds before giving up waiting for a probe response
	 * @throws IllegalArgumentException when parameters are out of range:
	 * 		destPort > 65535 || destPort < 0
	 * 		hMin < 1
	 * 		hMax < hMin
	 * 		timeout < 0
	 */
	public MultipathTraceroute(String address, Handler h, int destPort, short hMin, short hMax, int timeout, int attempts, int maxUnresponsiveHops)
		throws IllegalArgumentException {
		this.handler = h;

		setDestinationAddress(address);
		setDestPort(destPort);
		setHmin(hMin);
		setHmax(hMax);
		setTimeout(timeout);
		setAttempts(attempts);
		setMaxUnresponsiveHops(maxUnresponsiveHops);
	}

	/**
	 * @return the destination address as an @t InetAddress
	 */
	public InetAddress getDestinationAddress() {
		return this.destinationIP;
	}

	/**
	 * @param i the destination address to be set
	 */
	public void setDestinationAddress(InetAddress i) {
		this.destinationIP = i;
	}

	/**
	 * @param i dotted-decimal ip string or hostname of the destination address to be set
	 */
	public void setDestinationAddress(String i) {
		try {
			this.destinationIP = InetAddress.getByName(i);
		} catch (UnknownHostException e) {
			Log.w(TAG, "Lookup of " + i + " failed");
			e.printStackTrace();
		}
	}

	/**
	 * @return the destination port in use
	 */
	public int getDestPort() {
		return this.destinationPort;
	}

	/**
	 * @param port the destination port to be set
	 * @throws IllegalArgumentException if (port > 65535 || port < 0)
	 */
	public void setDestPort(int port) throws IllegalArgumentException {
		if (port > 65535 || port < 0)
			throw new IllegalArgumentException("Port number out of range");

		this.destinationPort = port;
	}

	/**
	 * @return the minimum hop number in use
	 */
	public short getHmin() {
		return this.hMin;
	}

	/**
	 * @param hMin the starting hop number to be set
	 * @throws IllegalArgumentException if hMin < 1
	 */
	public void setHmin(short hMin) throws IllegalArgumentException {
		if (hMin < 1)
			throw new IllegalArgumentException("Minimum hop number cannot be less than one");

		this.hMin = hMin;
	}

	/**
	 * @return the maximum hop number in use
	 */
	public short getHmax() {
		return this.hMax;
	}

	/**
	 * @param hMax the maximum hop number to be set
	 * @throws IllegalArgumentException if (hMax < hMin)
	 * @see setHmin()
	 */
	public void setHmax(short hMax) throws IllegalArgumentException {
		if (hMax < hMin)
			throw new IllegalArgumentException("Maximum hop number cannot be less than the minimum one");

		this.hMax = hMax;
	}

	/**
	 * @return get the current timeout for probe response waiting
	 */
	public int getTimeout() {
		return this.timeout;
	}

	/**
	 * @param timeout the timeout to be set for waiting for probe responses
	 * @throws IllegalArgumentException if (timeout < 0)
	 */
	public void setTimeout(int timeout) throws IllegalArgumentException {
		if (timeout < 0)
			throw new IllegalArgumentException("Timeout value must be positive");

		this.timeout = timeout;
	}

	/**
	 * @return get the current number of attempos for each interface in case of failure
	 */
	public int getAttempts() {
		return this.attempts;
	}

	/**
	 * @param attempts the number of attempts for each interface in case of failure
	 * @throws IllegalArgumentException if (attempts < 1)
	 */
	public void setAttempts(int attempts) throws IllegalArgumentException {
		if (attempts < 1)
			throw new IllegalArgumentException("Attempts number value must be greater than 0");

		this.attempts = attempts;
	}

	/**
	 * @return get the current number of unresponsive hops before stop
	 */
	public int getMaxUnresponsiveHops() {
		return this.maxUnresponsiveHops;
	}

	/**
	 * @param maxUnresponsiveHops the maximum number of consecutive unresponsive hops
	 * @throws IllegalArgumentException if (maxUnresponsiveHops < 1)
	 */
	public void setMaxUnresponsiveHops(int maxUnresponsiveHops) throws IllegalArgumentException {
		if (maxUnresponsiveHops < 1)
			throw new IllegalArgumentException("MaxUnresponsiveHops value must be greater than 0");

		this.maxUnresponsiveHops = maxUnresponsiveHops;
	}

	@Override
	public void run() {
		int unresponsiveHopsCount = 0;
		// Algorithm implementation

		boolean destReached = true;

		output(MSG_START);
		output(MSG_HOP, hMin-1);

		// First hop is usually not a multi-interface hop
		// with only the local interface.

		Set<Interface> localHop = new TreeSet<Interface>();

		localHop.add(Interface.getLocalInterface(true));
		hopInterfaceSet.append(hMin-1, localHop);

		Log.v(TAG, "MDA Starting");

		// For each hop ...

hoploop:for (int h = hMin; h <= hMax+1; h++) {

			Log.v(TAG, "Analyzing hop " + h);

			// Initialize the set of interfaces reachable at current hop

			Set<Interface> currentHopInterfaces = new TreeSet<Interface>();
			hopInterfaceSet.append(h, currentHopInterfaces);
			ArrayList<NextHops> finders = new ArrayList<NextHops>();

			// For each interface of the previous hop find its successors.
			// We use a thread for each Interface.

			Set<Interface> previousHopInterfaces = hopInterfaceSet.get(h-1); 
			if (previousHopInterfaces.size() > 1)
				output(MSG_HOP_IS_MIH);
			
			for (Interface r : previousHopInterfaces) {
				NextHops f = new NextHops(
					hopInterfaceSet, // Interface Set For Each Hop
					r, // Interface
					h, // Hop
					destinationIP,
					destinationPort,
					timeout,
					attempts
				);
				f.start();
				finders.add(f);
			}

			for (NextHops f : finders) {
				try {
					// Wait for result

					f.join();
					
					// Output the result
					output(MSG_INTERFACE, f.getInterface());

				}
				catch (InterruptedException e) {
					Log.i(TAG, "Interrupted HopFinder");
				}
			}

			if (this.isInterrupted()) {
				Log.v(TAG, "Algorithm is interrupted.");
				return;
			}

			// Check if paused() were called
			synchronized(pauseLock) {
				while (paused) {
					output(MSG_PAUSED);
					Log.v(TAG,"Paused");
					try { pauseLock.wait(); } catch (Exception e) {}
					output(MSG_RESUMED);
					Log.v(TAG,"Resumed");
				}
			}

			// Stop the algorithm after 'maxUnresponsiveHops' consecutive unresponsive hops
			if (currentHopInterfaces.size() == 0) {
				unresponsiveHopsCount++;
				Interface dummyHop = Interface.UnresponsiveHopFactory();
				currentHopInterfaces.add(dummyHop);
				if (unresponsiveHopsCount == maxUnresponsiveHops) {
					output(MSG_HOP, h);
					output(MSG_INTERFACE, dummyHop);
					Log.d(TAG, unresponsiveHopsCount + " consecutive unresponsive hops");
					break hoploop;
				}
			}
			else
				unresponsiveHopsCount = 0;
			
			if (h < hMax + 1)
				output(MSG_HOP, h);

			// Termination condition:
			// The current hop contains only the destinationIp

			destReached = true;
			for (Interface i : currentHopInterfaces) {
				if(i.getStringAddress().compareTo(destinationIP.getHostAddress()) != 0) {
					destReached = false;
					break;
				}
			}

			// Print the information found in the last step

			if (destReached) {
				for (Interface r : currentHopInterfaces)
					output(MSG_INTERFACE, r);
				output(MSG_DEST_REACHED);
				break;
			}
		}

		// MDA is terminated without reach the target

		if (!destReached) output(MSG_END);
	}

	/**
	 * Output function in MDA algorithm paper.
	 * Makes output of the algorithm available as message sent
	 * to the Handler instance specified in the constructor.
	 * @see MultipathTraceroute()
	 * @param r the interface to be output as next result
	 */

	private void output(int what, Object obj) {
		handler.obtainMessage(what, obj).sendToTarget();
	}
	private void output(int what) {
		handler.obtainMessage(what, null).sendToTarget();
	}

	/**
	 * Schedule the pause of the algorithm at the next pause point.
	 * Pause and Resume events are handled by output().
	 * @see output()
	 */

	public void pause() {
		synchronized (pauseLock) {
			paused = true;
		}
	}

	/**
	 * Resume the algorithm if it were paused.
	 * Pause and Resume events are handled by output().
	 * @see output()
	 */

	public synchronized void unpause() {
		synchronized (pauseLock) {
			paused = false;
			pauseLock.notify();
		}
	}

}
