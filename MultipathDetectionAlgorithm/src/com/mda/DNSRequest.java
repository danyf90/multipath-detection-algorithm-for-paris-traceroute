package com.mda;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.http.conn.util.InetAddressUtils;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

/**
 * Performs a DNS lookup for the given canonical address
 * 
 * @return true if an IPv4 address is found, false otherwise
 */
class DNSRequest extends AsyncTask<String, Void, Void> {
	private static final String TAG = "DNSRequest";
	
	public static final int DSN_LOOKUP_OK		= 0;
	public static final int DNS_LOOKUP_FAILED	= -1;
	public static final int DNS_LOOKUP_TIMEOUT	= -2;
	
	// DNS lookup timeout. millisecs.
	long timeout = 5000;
	Handler h;
	
	public DNSRequest(Handler h) {
		this.h = h;
	}
	
	@Override
	protected Void doInBackground(String... params) {
		if (params == null || params.length != 1)
			return error();
		
		DNSResolver dnsRes = new DNSResolver(params[0]);
        Thread t = new Thread(dnsRes);
        t.start();
        try {
			t.join(timeout);
		} catch (InterruptedException e) {
			error();
		}
        
        if (dnsRes.getComplete()) {
        	String inetAddr = dnsRes.get();
        	if (inetAddr != null)
        		h.obtainMessage(DSN_LOOKUP_OK, inetAddr).sendToTarget();
    	    else
    	    	error();
        } 
        else {
        	Log.d(TAG, "DNS lookup timeout occurred");
        	h.sendEmptyMessage(DNS_LOOKUP_TIMEOUT);
        } 
        	
        return null;
	}
	
	private Void error(){
		h.sendEmptyMessage(DNS_LOOKUP_FAILED);
		return null;
	}

}

class DNSResolver implements Runnable {
	private String domain;
    private String inetAddr = null;
    InetAddress[] addresses = null;
    String ipAddress = null;
    private boolean lookupComplete;
   
    public DNSResolver(String domain) {
        this.domain = domain;
        this.lookupComplete = false;
    }

    public void run() {
        try {
        	addresses = InetAddress.getAllByName(domain);
        } catch (UnknownHostException e) {
            set(null);
        } finally {
        	setComplete(true);
        }
        
        if (addresses != null) {
        	for (InetAddress address: addresses ) {
        		if (InetAddressUtils.isIPv4Address(address.getHostAddress())) {
        			ipAddress = address.getHostAddress();
        			break;
        		}
        	}
        }
        if ((ipAddress != null) && (!ipAddress.equals("67.215.65.132"))) // OpenDNS "fake" response to not found address
        	set(ipAddress);
        else
        	set(null);
    }

    public synchronized void set(String inetAddr) {
        this.inetAddr = inetAddr;
    }
    public synchronized String get() {
        return this.inetAddr;
    }
    public synchronized void setComplete(boolean complete) {
        this.lookupComplete = complete;
    }
    public synchronized boolean getComplete() {
        return this.lookupComplete;
    }
}