package com.mda;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.Toast;

/**
 * @author fabio
 *
 */
@SuppressWarnings("deprecation")
public class SettingsActivity extends PreferenceActivity 
	implements OnSharedPreferenceChangeListener, OnPreferenceClickListener {
	
	@SuppressWarnings("unused")
	private static final String TAG = "SettingsActivity";

	private enum Validity {
		VALID,
		NOT_VALID_SOURCE_PORT,
		NOT_VALID_DEST_PORT,
		NOT_VALID_INITIAL_TTL,
		NOT_VALID_MAX_TTL,
		NOT_VALID_TTL_RANGE,
		NOT_VALID_TIMEOUT,
		NOT_VALID_ATTEMPTS,
		NOT_VALID_FORMAT,
		NOT_VALID_MAX_UNRESP
	}
	
	PreferenceScreen ps;
	String oldValue;
	private MenuItem restoreDefault;
	
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            
            ps = getPreferenceScreen();
            initPreferences();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings, menu);

		restoreDefault = menu.findItem(R.id.menu_item_restore_default);

		restoreDefault.setOnMenuItemClickListener(this.resetSettings);
		
		return true;
	}

    @Override
    protected void onResume() {
        super.onResume();
        // Set up a listener
        ps.getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener
        ps.getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Check if the new settings are allowed and restore the previous ones if needed
     * @param sharedPreferences	SharedPreferences Object
     * @param key				key of the modified setting
     */
    @Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    	if (oldValue != null) { // avoid to reevaluate settings after the restore
    		int msgId = 0;
	    		
    		Validity v = isValid(sharedPreferences);

    		switch(v) {
    		case VALID:
	    		setPreferenceSummary(findPreference(key));
    			return;
    			
    		case NOT_VALID_FORMAT:
    			msgId = R.string.mda_error_format;
    			break;
    			
    		case NOT_VALID_SOURCE_PORT:
    			msgId = R.string.mda_error_source_port;
    			break;

    		case NOT_VALID_DEST_PORT:
    			msgId = R.string.mda_error_dest_port;
    			break;

    		case NOT_VALID_INITIAL_TTL:
    			msgId = R.string.mda_error_min_ttl;
    			break;

    		case NOT_VALID_MAX_TTL:
    			msgId = R.string.mda_error_max_ttl;
    			break;

    		case NOT_VALID_TTL_RANGE:
    			msgId = R.string.mda_error_ttl_range;
    			break;

    		case NOT_VALID_TIMEOUT:
    			msgId = R.string.mda_error_timeout;
    			break;

    		case NOT_VALID_ATTEMPTS:
    			msgId = R.string.mda_error_attempts;
    			break;
    			
    		case NOT_VALID_MAX_UNRESP:
    			msgId = R.string.mda_error_max_unresponsive_hops;
    			break;
    		}
    		
			Toast.makeText(this, getString(msgId), Toast.LENGTH_SHORT).show();
			restoreValue(sharedPreferences, key);
    	}
	}
    
    private void restoreValue(SharedPreferences sharedPreferences, String key) {
    	if (sharedPreferences == null || key == null)
    		return;

		Preference p = ps.findPreference(key);
		if (p instanceof EditTextPreference)
			((EditTextPreference) p).setText(oldValue);
		
		Editor e = sharedPreferences.edit();
		e.putString(key, oldValue);
		oldValue = null;
		e.commit();
		
    }
	
    /**
	 * Store the old value, and select all the text
	 * @param p	clicked Preference object
	 */
	@Override
    public boolean onPreferenceClick(Preference p) {
        if (p instanceof EditTextPreference) {
        	EditTextPreference etp = (EditTextPreference) p;
        	
        	oldValue = etp.getText();
        	etp.getEditText().selectAll();
        }
        return true;
    }
	
	/**
	 * Set the preference summary to the preference value
	 * @param p	Preference objet
	 */
	protected void setPreferenceSummary(Preference p) {
		if (p instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) p;
            etp.setSummary(etp.getText());
        }
    }
	
	protected void initPreferences() {
		Preference p;
		
		for (int i = 0; i < ps.getPreferenceCount(); i++) {
			p = ps.getPreference(i);
			
			if (p instanceof PreferenceCategory) {
	    		PreferenceCategory pref = (PreferenceCategory) p;
	    		for (int j = 0; j < pref.getPreferenceCount(); j++)
	    			initPreference(pref.getPreference(j));
	    	}
			else
				initPreference(p);
        }
	}
	
	protected void initPreference(Preference p) {
		p.setOnPreferenceClickListener(this);
		setPreferenceSummary(p);
	}
	
	/**
	 * Check the validity of the settings
	 * @param sharedPreferences SharedPreferences object
	 * @return result of the check as Validity enum
	 */
	protected Validity isValid(SharedPreferences sharedPreferences) {
		int destPort, initialTTL, maxTTL, timeout, attempts, sourcePort, maxUnresp;
		
		try {
			destPort = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_destination_port), "-1"));
			
			initialTTL = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_initial_ttl), "-1"));
			
			maxTTL = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_max_ttl), "-1"));
			
			timeout = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_timeout), "-1"));
			
			attempts = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_attempts), "-1"));
			
			sourcePort = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_source_port), "-1"));
			
			maxUnresp = Integer.parseInt(sharedPreferences.getString(getString(
					R.string.prefs_max_unresponsive_hops), "-1"));
			
		} catch (NumberFormatException nfe) {
			return Validity.NOT_VALID_FORMAT;
		}

		if (sourcePort <= 1024 || sourcePort > 65535)
			return Validity.NOT_VALID_SOURCE_PORT;
		
		if (destPort < 1 || destPort > 65535)
			return Validity.NOT_VALID_DEST_PORT;
		
		if (initialTTL < 1 || initialTTL > 255)
			return Validity.NOT_VALID_INITIAL_TTL;
		
		if (maxTTL < 1 || maxTTL > 255)
			return Validity.NOT_VALID_MAX_TTL;
		
		if (maxTTL < initialTTL)
			return Validity.NOT_VALID_TTL_RANGE;
		
		if (timeout < 1)
			return Validity.NOT_VALID_TIMEOUT;
		
		if (attempts < 1)
			return Validity.NOT_VALID_ATTEMPTS;
		
		if (maxUnresp < 1)
			return Validity.NOT_VALID_MAX_UNRESP;
		
		return Validity.VALID;
	}
	
	/**
	 * Restore default settings
	 */
	OnMenuItemClickListener resetSettings = new OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			
			showConfirmDialog();
			
			return true;
		}

		private void showConfirmDialog() {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int which) {
                         switch (which) {
                         case DialogInterface.BUTTON_POSITIVE:
                                Toast.makeText(SettingsActivity.this, R.string.prefs_reset_restored,
                                              Toast.LENGTH_LONG).show();
                                resetPreferences();
                                break;

                         default:
                        	 break;
                         }
                   }
            };

            (new AlertDialog.Builder(SettingsActivity.this))
	            .setMessage(R.string.prefs_reset_title)
	            .setPositiveButton(R.string.prefs_reset_yes, dialogClickListener)
	            .setNegativeButton(R.string.prefs_reset_no, dialogClickListener).show();
     	}

		private void resetPreferences() {
			setTextPreference(R.string.prefs_destination_port, getString(R.string.mda_dest_port_default));
			setTextPreference(R.string.prefs_source_port, getString(R.string.mda_source_port_default));
			setTextPreference(R.string.prefs_initial_ttl, getString(R.string.mda_initial_ttl_default));
			setTextPreference(R.string.prefs_max_ttl, getString(R.string.mda_max_ttl_default));
			setTextPreference(R.string.prefs_timeout, getString(R.string.mda_max_resp_wait_default));
			setTextPreference(R.string.prefs_attempts, getString(R.string.mda_max_attempts_default));
			setTextPreference(R.string.prefs_max_unresponsive_hops, getString(R.string.mda_max_unresponsive_hops_default));
			setBooleanPreference(R.string.prefs_show_port_number, Boolean.parseBoolean(getString(R.string.mda_show_port_number_default)));
			setBooleanPreference(R.string.prefs_show_all_flows, Boolean.parseBoolean(getString(R.string.mda_show_all_flows_default)));
		}
	
		private void setTextPreference(int id, String value) {
			Editor e;
			
			((EditTextPreference) ps.findPreference(getString(id))).setText(value);
			
			e = ps.getSharedPreferences().edit();
			e.putString(getString(id), value);
			e.commit();
			
			setPreferenceSummary(findPreference(getString(id)));
		}

		private void setBooleanPreference(int id, Boolean value) {
			Editor e;
			
			((CheckBoxPreference) ps.findPreference(getString(id))).setChecked(value);
			
			e = ps.getSharedPreferences().edit();
			e.putBoolean(getString(id), value);
			e.commit();
			
			setPreferenceSummary(findPreference(getString(id)));
		}
	};

}
