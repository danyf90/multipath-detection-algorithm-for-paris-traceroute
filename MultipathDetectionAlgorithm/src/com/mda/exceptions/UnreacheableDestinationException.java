package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

public class UnreacheableDestinationException extends ProbeException {

	private static final long serialVersionUID = 6011057259320552637L;

	public UnreacheableDestinationException(ProbeResult r, ProbeParameters p) {
		super(r,p);
	}
	
	@Override
	public String getMessage() {
		return "Probed destination (" + getProbeParameters().getDestIp() + ") cannot be reached";
	}

}
