/**
 * 
 */
package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

/**
 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
 *
 */
public class AlreadyInUseException extends ProbeException {

	private static final long serialVersionUID = 6002455415962851510L;

	public AlreadyInUseException(ProbeResult r, ProbeParameters p) {
		super(r,p);
	}
	
	@Override
	public String getMessage() {
		return "Bind failed, port " + getProbeParameters().getSourcePort() + "is already in use";
	}

}
