package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

public class UnexpectedIcmpMessageException extends ProbeException {

	private static final long serialVersionUID = -4749407230411679754L;

	public UnexpectedIcmpMessageException(ProbeResult r, ProbeParameters p) {
		super(r,p);
	}
	
	@Override
	public String getMessage() {
		return "Unexpected ICMP message received after probing";
	}

}
