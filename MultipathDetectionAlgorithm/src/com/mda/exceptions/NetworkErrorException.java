package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

public class NetworkErrorException extends ProbeException {

	private static final long serialVersionUID = -9076162473376644924L;

	public NetworkErrorException(ProbeResult r, ProbeParameters p) {
		super(r,p);
	}
	
	@Override
	public String getMessage() {
		return "Network error occurred while initializing the probe";
	}
}
