/**
 * 
 */
package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

/**
 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
 *
 */
public class ProbeException extends Exception {

	private static final long serialVersionUID = 7834699945498381867L;
	private ProbeResult r;
	private ProbeParameters p;
	
	/**
	 * @param r 
	 * @param p 
	 */
	public ProbeException(ProbeResult r, ProbeParameters p) {
		this.r = r;
		this.p = p;
	}

	/**
	 * @return the ProbeResult object that may contain erroneous info
	 */
	public ProbeResult getProbeResult() {
		return r;
	}
	
	/**
	 * @return the ProbeParameters object that has been used to request the probing
	 */
	public ProbeParameters getProbeParameters() {
		return p;
	}	
}
