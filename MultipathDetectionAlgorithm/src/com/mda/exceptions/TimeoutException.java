package com.mda.exceptions;

import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

public class TimeoutException extends ProbeException {

	private static final long serialVersionUID = -583833631420820265L;

	public TimeoutException(ProbeResult r, ProbeParameters p) {
		super(r,p);
	}

	@Override
	public String getMessage() {
		return "Timeout occurred";
	}

}
