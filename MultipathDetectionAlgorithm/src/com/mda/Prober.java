package com.mda;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.util.Log;

import com.mda.exceptions.*;
import com.mda.types.Interface;
import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

/**
 * Interface to the native UDP Prober, implemented in C++.
 */
public class Prober extends Thread {
	
	private static final String TAG = "Prober";
	protected ProbeParameters p;
	
	private Interface r;
	private int result;

	/**
	 * Constructs a @t Prober with the given parameters.
	 * @param p	probe parameters
	 */
	
	public Prober(ProbeParameters p) {
		setParameters(p);
	}

	public void setParameters(ProbeParameters p) {
		this.p = p;
	}

	public Interface getInterface() {
		return r;
	}

	public int getResult() {
		return result;
	}

	/**
	 * Sends the probe and waits for a response
	 * @return the result of the probe
	 * @throws UnreacheableDestinationException 
	 * @throws UnexpectedIcmpMessageException 
	 * @throws NetworkErrorException 
	 * @throws AlreadyInUseException 
	 * @throws TimeoutException 
	 * @throws DestinationReachedException 
	 */
	
	public ProbeResult send() throws UnreacheableDestinationException,
									UnexpectedIcmpMessageException,
									NetworkErrorException,
									AlreadyInUseException,
									TimeoutException {
		
		ProbeResult r = sendProbe(p);
		
		switch (r.getCode()) {
		case ProbeResult.TIMEOUT_OCCURRED:
			throw new TimeoutException(r,p);
		case ProbeResult.UNEXPECTED_ICMP_MSG:
			throw new UnexpectedIcmpMessageException(r,p);
		case ProbeResult.ICMP_DEST_UNREACH:
			throw new UnreacheableDestinationException(r,p);
		case ProbeResult.NETWORK_ERROR:
			throw new NetworkErrorException(r,p);
		case ProbeResult.ALREADY_IN_USE_ERROR:
			throw new AlreadyInUseException(r,p);
		}
		
		return r;
	}

	/**
	 * Native function that implements the prober
	 * @param p	probe parameters
	 * @return probe response
	 */
	
	private native ProbeResult sendProbe(ProbeParameters p);
	
	/**
	 * Sends a traceroute probe with TTL equal to @p ttl, with source port
	 * @p flowPort, with a maximum of @p retransmissions retransmissions
	 * @param ttl				probe TTL
	 * @param flowPort			probe source port
	 * @param retransmissions	maximum number of retransmissions
	 * @return
	 */
	public void run() {
		InetAddress	address;
		ProbeResult probeResult = null;
		r = null;
		
		// Send the probe and get the result or retransmit the packet
		for (int i = 0; i < p.getAttempts(); i++) {
			
			try { 
				probeResult = send();
				
				try {
					address = InetAddress.getByName(probeResult.getInterfaceIp());
					r = new Interface(address);
				} catch (UnknownHostException e) {
					Log.w(TAG, "Lookup of " + probeResult.getInterfaceIp() + " failed");
					e.printStackTrace();
				}
				return;
			}
			catch (ProbeException e) {
				probeResult = e.getProbeResult();
				
				if (probeResult.getCode() == ProbeResult.ALREADY_IN_USE_ERROR)
					break; // in case of ALREADY_IN_USE_ERROR don't perform retransmission
			}
		}
		
		r = Interface.UnresponsiveHopFactory();
		result = probeResult.getCode();
	}
	
	static {
	    System.loadLibrary("prober");
	}
}
