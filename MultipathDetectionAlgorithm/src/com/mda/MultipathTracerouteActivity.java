package com.mda;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;

import com.mda.types.Interface;

/**
 * MultipathTracerouteActivity starts MDA Algorithm and print the result.
 * 
 * @author andrea
 * 
 */
public class MultipathTracerouteActivity extends ActionBarActivity {
	private static final String TAG = "MultipathTracerouteActivity";

	private static final String NO_BALANCER_COLOR = "#000000";
	private static final String PER_FLOW_BALANCER_COLOR = "#5555ff";
	private static final String PER_PACKET_BALANCER_COLOR = "#ff5555";

	private static final int PAUSE_ICON = android.R.drawable.ic_media_pause;
	private static final int PAUSING_ICON = android.R.drawable.ic_menu_close_clear_cancel;
	private static final int START_ICON = android.R.drawable.ic_media_play;

	private static final long[] DEST_REACHED_PATTERN = {100, 150, 50, 150, 50, 150};
	private static final long[] DEST_NOT_REACHED_PATTERN = {100, 1000};

	TextView message;
	ScrollView sv;
	TableLayout multipathTracerouteTable;
	LayoutInflater inflater;
	ProgressBar progress;

	String ipAddress, shareText, shareTextLB;
	int currentHop, destPort, initialTTL, maxTTL, maxResp, maxAttempts, maxUnresponsiveHops;
	Boolean showPortNumber, showAllFlows;
	
	private MenuItem shareItem, playPauseItem, restartItem;
	private ShareActionProvider mShareActionProvider;
	private Vibrator vibrator;
	Intent shareIntent = new Intent(Intent.ACTION_SEND);

	MultipathTraceroute tracer;
	private SparseIntArray portMap;
	private int flowKey;
	private boolean isMIH;

	private static class MyHandler extends Handler {
		private final WeakReference<MultipathTracerouteActivity> wActivity;

		public MyHandler(MultipathTracerouteActivity activity) {
			wActivity = new WeakReference<MultipathTracerouteActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			MultipathTracerouteActivity activity = wActivity.get();
			if (activity == null || msg == null)
				return;

			switch (msg.what) {
			case MultipathTraceroute.MSG_START:
				activity.progress.setVisibility(View.VISIBLE);
				activity.disableShareMenu();
				Log.v(TAG, "MultipathTraceroute starts.");
				break;

			case MultipathTraceroute.MSG_HOP:
				if (msg.obj == null)
					return;

				activity.isMIH = false;
				activity.currentHop = (Integer) msg.obj;
				if (activity.currentHop == activity.initialTTL - 1)
					activity.addTableHop(0);
				else
					activity.addTableHop(activity.currentHop);
				break;
				
			case MultipathTraceroute.MSG_HOP_IS_MIH:
				activity.isMIH = true;
				break;

			case MultipathTraceroute.MSG_INTERFACE:
				if (msg.obj == null)
					return;

				Interface discoveredHop = (Interface) msg.obj;
				activity.addTableRow(discoveredHop);
				break;

			case MultipathTraceroute.MSG_DEST_REACHED:

				activity.progress.setVisibility(View.INVISIBLE);
				
				activity.message.setText(R.string.dest_reached);

				activity.playPauseItem.setVisible(false);
				activity.restartItem.setVisible(true);

				activity.vibrator.vibrate(DEST_REACHED_PATTERN, -1);

				Log.v(TAG, "Destination reached.");
				activity.enableShareMenu();
				break;

			case MultipathTraceroute.MSG_END:

				activity.progress.setVisibility(View.INVISIBLE);
				
				activity.message.setText(R.string.dest_unreached);
				
				activity.playPauseItem.setVisible(false);
				activity.restartItem.setVisible(true);

				activity.vibrator.vibrate(DEST_NOT_REACHED_PATTERN, -1);

				Log.v(TAG, "Destination not reached.");
				activity.enableShareMenu();
				break;

			case MultipathTraceroute.MSG_PAUSED:
				
				activity.progress.setVisibility(View.INVISIBLE);
				
				activity.message
						.setText(R.string.multipath_traceroute_message_paused);
				
				activity.playPauseItem
						.setTitle(R.string.menu_item_resume)
						.setIcon(START_ICON)
						.setOnMenuItemClickListener(activity.requestResume);
				
				activity.restartItem.setVisible(true);
				
				activity.enableShareMenu();
				break;

			case MultipathTraceroute.MSG_RESUMED:
				
				activity.progress.setVisibility(View.VISIBLE);
				
				activity.message
						.setText(R.string.multipath_traceroute_message_running);
				
				activity.playPauseItem
						.setTitle(R.string.menu_item_pause)
						.setIcon(PAUSE_ICON)
						.setOnMenuItemClickListener(activity.requestPause);
				
				activity.restartItem.setVisible(false);
				
				activity.disableShareMenu();
				break;

			default:
				Log.e(TAG, "Unexpected Message");
			}
		}
	};

	public final MyHandler resultsHandler = new MyHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multipath_traceroute);

		TextView destination = (TextView) findViewById(R.id.multipath_traceroute_destination);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
			destination.setText("No valid destination");
			return;
		} else {
			ipAddress = extras
					.getString(getString(R.string.key_destination_ip));
			destination.setText(ipAddress);
		}

		loadPreferences();

		sv = (ScrollView) findViewById(R.id.multipath_traceroute_scroll_view);
		sv.setVerticalScrollBarEnabled(true);

		multipathTracerouteTable = (TableLayout) findViewById(R.id.multipath_traceroute_table_body);
		message = (TextView) findViewById(R.id.multipath_traceroute_message);
		progress = (ProgressBar) findViewById(R.id.progressBar1);

		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		initializeShareText();
		
		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		
		if (!showPortNumber) {
			portMap = new SparseIntArray();
			flowKey = 0;
		}
		
		tracer = new MultipathTraceroute(ipAddress, resultsHandler, destPort,
				(short) initialTTL, (short) maxTTL, maxResp, maxAttempts, maxUnresponsiveHops);
		tracer.start();
	}

	/**
	 * Share results 
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.multipath_traceroute, menu);

		playPauseItem = menu.findItem(R.id.menu_item_play_pause);
		playPauseItem
			.setTitle(R.string.menu_item_pause)
			.setIcon(PAUSE_ICON)
			.setOnMenuItemClickListener(requestPause);
		restartItem = menu.findItem(R.id.menu_item_restart);
		restartItem.setOnMenuItemClickListener(requestRestart);
		
		shareItem = menu.findItem(R.id.menu_item_share);
		
		shareIntent
			.setType("text/plain")
			.putExtra(Intent.EXTRA_SUBJECT, "MultipathDetectionAlgorithm");
		
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
		setShareIntent("");
		
		return true;
	}
	
	private void initializeShareText() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US);
		Date date = new Date();
		System.out.println();
		shareText = "MDA Output for ip " + ipAddress + " @ " + dateFormat.format(date) + "\n";
		shareText += "\nTTL\tIP Address\t\tFlows\n";
	}

	private void setShareIntent(String text) {
		if (mShareActionProvider != null) {
			shareIntent.putExtra(Intent.EXTRA_TEXT, text);
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}
	
	public void enableShareMenu() {
		shareItem.setVisible(true);
	}

	public void disableShareMenu() {
		shareIntent.putExtra(Intent.EXTRA_TEXT, "");
		if (shareItem != null) {
			shareItem.setVisible(false);
		}
	}

	private void loadPreferences() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		destPort = Integer.valueOf(settings.getString(
				getString(R.string.prefs_destination_port),
				getString(R.string.mda_dest_port_default)));

		initialTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_initial_ttl),
				getString(R.string.mda_initial_ttl_default)));

		maxTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_ttl),
				getString(R.string.mda_max_ttl_default)));

		maxResp = Integer.valueOf(settings.getString(
				getString(R.string.prefs_timeout),
				getString(R.string.mda_max_resp_wait_default)));

		maxAttempts = Integer.valueOf(settings.getString(
				getString(R.string.prefs_attempts),
				getString(R.string.mda_max_attempts_default)));
		
		maxUnresponsiveHops = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_unresponsive_hops),
				getString(R.string.mda_max_unresponsive_hops_default)));

		showPortNumber = Boolean.valueOf(settings.getBoolean(getString(R.string.prefs_show_port_number),
				Boolean.valueOf(getString(R.string.mda_show_port_number_default))));
		
		showAllFlows = Boolean.valueOf(settings.getBoolean(getString(R.string.prefs_show_all_flows),
				Boolean.valueOf(getString(R.string.mda_show_all_flows_default))));

	}

	@Override
	protected void onPause() {
		super.onPause();
		tracer.pause();
	}

	@Override
	protected void onDestroy() {
		tracer.interrupt();
		super.onDestroy();
	}

	/**
	 * Inflate a table row by xml layout, populate fields and add to the
	 * scrollable table
	 * 
	 * @param id
	 *            TTL
	 * @param respIP
	 *            interface hop
	 * @param RTT
	 *            response TTL from Prober
	 */
	public void addTableRow(Interface i) {
		shareTextLB = "";
		TableRow newRow = (TableRow) inflater.inflate(
				R.layout.multipath_traceroute_row, null);
		if ((currentHop-initialTTL) % 2 != 0 || currentHop == initialTTL-1)
			newRow.setBackgroundColor(Color.parseColor("#cccccc"));

		TextView tvIP = (TextView) newRow
				.findViewById(R.id.multipath_traceroute_table_attribute_ip);
		TextView tvFlowID = (TextView) newRow
				.findViewById(R.id.multipath_traceroute_table_attribute_flow_id);

		tvIP.setText("\t" + i.getStringAddress());
		switch(i.getLoadBalancingType()) {
		case NO_BALANCE:
			tvIP.setTextColor(Color.parseColor(NO_BALANCER_COLOR));
			break;
		case PER_FLOW_BAL:
			tvIP.setTextColor(Color.parseColor(PER_FLOW_BALANCER_COLOR));
			shareTextLB = "(PF_LB)";
			break;
		case PER_PACKET_BAL:
			tvIP.setTextColor(Color.parseColor(PER_PACKET_BALANCER_COLOR));
			shareTextLB = "(PP_LB)";
			break;
		default:
			break;
		
		}
		
		shareText += "\t" + i.getStringAddress() + " " + shareTextLB;
		
		if ((!isMIH && !showAllFlows)|| i.getFlowPort(currentHop) == -1) {
			tvFlowID.setText(" ");
			shareText += "\t\t" + " " + "\n";
		} else {
			String portsString = "";
			List<Integer> flowIDs, ports;
			
			ports = i.getFlowPorts(currentHop);
			
			if (showPortNumber)
				flowIDs = ports;
			else {
				flowIDs = new ArrayList<Integer>();
				for (int j = 0; j < ports.size(); j++) {
					Integer flow = ports.get(j);
					
					if (portMap.indexOfKey(flow) < 0)
						portMap.put(flow, ++flowKey);
						
					flowIDs.add(portMap.get(flow)); 	
				}
			}
			
			Collections.sort(flowIDs);
			
			for (Integer id : flowIDs)
				portsString += id + ", ";
			
			tvFlowID.setText(portsString.substring(0, portsString.length() - 2)); // remove final ", "
			
			shareText += "\t\t" + portsString.substring(0, portsString.length() - 2) + "\n";
		}
		
		setShareIntent(shareText);
		
		multipathTracerouteTable.addView(newRow);
		scrollDown();
	}

	/**
	 * Add an "hop row" to the table with the current hop number
	 */
	public void addTableHop(int h) {
		View newRow = inflater.inflate(R.layout.multipath_traceroute_row, null);
			
		if ((h-initialTTL) % 2 != 0 || h == 0)
			newRow.setBackgroundColor(Color.parseColor("#cccccc"));

		TextView tvIP = (TextView) newRow
				.findViewById(R.id.multipath_traceroute_table_attribute_ip);
		TextView tvFlowID = (TextView) newRow
				.findViewById(R.id.multipath_traceroute_table_attribute_flow_id);

		tvIP.setText(String.valueOf(h) + ":");
		tvFlowID.setText("");
		
		shareText += "\n" + String.valueOf(h) + ":" + "\n";
		setShareIntent(shareText);
				
		multipathTracerouteTable.addView(newRow);
		scrollDown();
	}

	/**
	 *  Scroll the view after the update 
	 */
	public void scrollDown() {
		sv.post(new Runnable() {
	        @Override
	        public void run() {
	            sv.fullScroll(ScrollView.FOCUS_DOWN);
	        }
	    });
		
	}
	
	/**
	 * Pause button behavior: ask the algorithm to pause
	 */
	OnMenuItemClickListener requestPause = new OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			item
				.setTitle(R.string.menu_item_pausing)
				.setIcon(PAUSING_ICON);
			
			tracer.pause();
			
			return false;
		}
	};

	/**
	 * Pause button behavior: ask the algorithm to resume
	 */
	OnMenuItemClickListener requestResume = new OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			tracer.unpause();
			
			item
				.setTitle(R.string.menu_item_pause)
				.setIcon(PAUSE_ICON);
			
			return false;
		}
	};
	
	/**
	 * Restart button behavior: ask the algorithm to restart
	 */
	OnMenuItemClickListener requestRestart = new OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			multipathTracerouteTable.removeAllViews();
			setShareIntent("");
			
			tracer = new MultipathTraceroute(ipAddress, resultsHandler,
					destPort, (short) initialTTL, (short) maxTTL, maxResp,
					maxAttempts, maxUnresponsiveHops);
			tracer.start();
			
			progress.setVisibility(View.VISIBLE);
			
			message.setText(R.string.multipath_traceroute_message_running);
			
			item.setVisible(false);
			playPauseItem
				.setTitle(R.string.menu_item_pause)
				.setIcon(PAUSE_ICON)
				.setOnMenuItemClickListener(requestPause)
				.setVisible(true);

			initializeShareText();

			if (!showPortNumber) {
				portMap.clear();
				flowKey = 0;
			}
			
			return false;
		}
	};
}