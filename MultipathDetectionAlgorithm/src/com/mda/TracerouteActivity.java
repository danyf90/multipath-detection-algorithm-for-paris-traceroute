package com.mda;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.mda.types.ProbeResult;

/**
 * TracerouteActivity starts traceroute and print the result.
 * 
 * @author andrea
 * 
 */
public class TracerouteActivity extends ActionBarActivity {
	private static final String TAG = "TracerouteActivity";

	private static final long[] DEST_REACHED_PATTERN = {50, 150, 50, 150, 50, 150};
	private static final long[] DEST_NOT_REACHED_PATTERN = {50, 1000};
	
	private TextView message;
	private ScrollView sv;
	private TableLayout tr_table;
	private LayoutInflater inflater;
	private ProgressBar progress;
	
	String ipAddress;
	int sourcePort, destPort, initialTTL, maxTTL, maxResp, maxAttempts, maxUnresponsiveHops;
	private Vibrator vibrator;
	private Traceroute traceroute;
	
	private static class MyHandler extends Handler {
		private final WeakReference<TracerouteActivity> wActivity;

		private int h = 0;
		
		public MyHandler(TracerouteActivity activity) {
			wActivity = new WeakReference<TracerouteActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			TracerouteActivity activity = wActivity.get();
			if (activity == null || msg == null)
				return;
			
			switch(msg.what) {
			case Traceroute.MSG_START:
				
				activity.progress.setVisibility(View.VISIBLE);
				Log.v(TAG, "Traceroute starts");
				break;
			
			case Traceroute.MSG_INTERFACE:
				if (msg.obj == null)
					return;

				ProbeResult pr = (ProbeResult) msg.obj;

				activity.addTableRow(h, pr.getInterfaceIp(), pr.getRtt());
				
				if (h == 0)
					h = activity.initialTTL;
				else
					h++;
				break;
				
			case Traceroute.MSG_DEST_REACHED:

				activity.progress.setVisibility(View.INVISIBLE);
				activity.message.setText(R.string.dest_reached);
				activity.scrollDown();
				
				activity.vibrator.vibrate(DEST_REACHED_PATTERN, -1);
				Log.v(TAG, "Destination reached");
				break;
			
			case Traceroute.MSG_END:

				activity.progress.setVisibility(View.INVISIBLE);
				activity.message.setText(R.string.dest_unreached);
				activity.scrollDown();

				activity.vibrator.vibrate(DEST_NOT_REACHED_PATTERN, -1);
				Log.v(TAG, "Destination not reached");
				break;

			default:
				Log.e(TAG, "Unexpected Message");
			}
		}
	};

	public final MyHandler resultsHandler = new MyHandler(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_traceroute);
		
		TextView parameters = (TextView) findViewById(R.id.traceroute_destination);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
			parameters.setText("No valid destination");
			return;
		}
		
		loadPreferences();
			 
		ipAddress = extras.getString(getString(R.string.key_destination_ip));
		parameters.setText(ipAddress);
			
		sv = (ScrollView) findViewById(R.id.scrollViewTR);
		sv.setVerticalScrollBarEnabled(true);
		sv.setScrollbarFadingEnabled(false);
		
		tr_table = (TableLayout) findViewById(R.id.traceroute_table_body);
		message = (TextView) findViewById(R.id.traceroute_message);
		progress = (ProgressBar) findViewById(R.id.progressBar1);

		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		
		traceroute = new Traceroute(
				ipAddress,
				resultsHandler,
				sourcePort,
				destPort,
				(short) initialTTL,
				(short) maxTTL,
				maxResp,
				maxAttempts,
				maxUnresponsiveHops);

		traceroute.start();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		traceroute.interrupt();
	}

	
	private void loadPreferences() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		
		sourcePort =  Integer.valueOf(settings.getString(
				getString(R.string.prefs_source_port),
				getString(R.string.mda_source_port_default)
				));
		
		destPort = Integer.valueOf(settings.getString(
				getString(R.string.prefs_destination_port),
				getString(R.string.mda_dest_port_default)
				));
		
		initialTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_initial_ttl),
				getString(R.string.mda_initial_ttl_default)
				));
		
		maxTTL = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_ttl),
				getString(R.string.mda_max_ttl_default)
				));
		
		maxResp = Integer.valueOf(settings.getString(
				getString(R.string.prefs_timeout),
				getString(R.string.mda_max_resp_wait_default)
				));
		
		maxAttempts = Integer.valueOf(settings.getString(
				getString(R.string.prefs_attempts),
				getString(R.string.mda_max_attempts_default)
				));

		maxUnresponsiveHops = Integer.valueOf(settings.getString(
				getString(R.string.prefs_max_unresponsive_hops),
				getString(R.string.mda_max_unresponsive_hops_default)));
	}

	/**
	 * Inflate a table row by xml layout, populate fields and add to the scrollable
	 * table
	 * 
	 * @param ttl		TTL
	 * @param respIP	interface hop
	 * @param RTT		response TTL from Prober
	 */
	public void addTableRow(int ttl, String respIP, long RTT) {
		View newRow = inflater.inflate(R.layout.traceroute_row, null);
		if ((ttl-initialTTL) % 2 != 0 || ttl == 0)
			newRow.setBackgroundColor(Color.parseColor("#cccccc"));

		((TextView) newRow.findViewById(R.id.traceroute_table_attribute_ttl))
			.setText(Integer.toString(ttl));
		((TextView) newRow.findViewById(R.id.traceroute_table_attribute_ip))
			.setText(respIP);
		((TextView) newRow.findViewById(R.id.traceroute_table_attribute_rtt))
			.setText(Double.toString(RTT / 1000.0));

		tr_table.addView(newRow);
		scrollDown();
	}
	
	/**
	 *  Scroll the view after the update 
	 */
	public void scrollDown() {
		sv.post(new Runnable() {
	        @Override
	        public void run() {
	            sv.fullScroll(ScrollView.FOCUS_DOWN);
	        }
	    });
		
	}
}
