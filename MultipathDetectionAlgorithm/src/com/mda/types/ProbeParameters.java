package com.mda.types;

import android.util.Patterns;

/**
 * Parameters of the probe.
 * @param destIp		probe destination IP in the form a.b.c.d
 * @param sourcePort	probe source port, if -1 it is chosen by the system
 * @param destPort		probe destination port
 * @param ttl			probe Time To Live
 * @param ttl			probe timeout
 */
public class ProbeParameters {
	protected String	destIp;
	protected int		sourcePort;
	protected int		destPort;
	protected int		ttl;
	protected int		timeout;
	protected int		attempts;
	
	public ProbeParameters(String destIp, int sourcePort, int destPort, int ttl, int timeout, int attempts) {
		setDestIp(destIp);
		setSourcePort(sourcePort);
		setDestPort(destPort);
		setTtl(ttl);
		setTimeout(timeout);
		setAttempts(attempts);
	}
	
	/**
	 * @return probe destination ip
	 */
	public String getDestIp() {
		return destIp;
	}

	/**
	 * @param destIp	destination address to be set
	 * @throws IllegalArgumentException if @p destIp port is not a valid IP address
	 */
	public void setDestIp(String destIp) throws IllegalArgumentException {
		if (destIp == null || Patterns.IP_ADDRESS.matcher(destIp).matches() == false)
			throw new IllegalArgumentException("Destination IP must be in the"
					+ " form a.b.c.d");

		this.destIp = destIp;
	}

	/**
	 * @return probe source port
	 */
	public int getSourcePort() {
		return sourcePort;
	}

	/**
	 * @param port	source port to be set
	 * @throws IllegalArgumentException if (port < 0 || port > 65535)
	 */
	public void setSourcePort(int sourcePort) throws IllegalArgumentException {
		if (sourcePort < 0 || sourcePort > 65535)
			throw new IllegalArgumentException("Source port must be in the "
					+ "range [-1, 65535]");

		this.sourcePort = sourcePort;
	}

	/**
	 * @return probe destination port
	 */
	public int getDestPort() {
		return destPort;
	}

	/**
	 * @param port	destination port to be set
	 * @throws IllegalArgumentException if (port < 0 || port > 65535)
	 */
	public void setDestPort(int destPort) throws IllegalArgumentException {
		if (destPort < 0 || destPort > 65535)
			throw new IllegalArgumentException("Destination port must be in "
					+ "the range [0, 65535]");

		this.destPort = destPort;
	}

	/**
	 * @return probe Time To Live
	 */
	public int getTtl() {
		return ttl;
	}

	/**
	 * @param ttl	ttl to be set
	 * @throws IllegalArgumentException if (ttl < 1 || ttl > 255)
	 */
	public void setTtl(int ttl) throws IllegalArgumentException {
		if (ttl < 1 || ttl > 255)
			throw new IllegalArgumentException("TTL must be in the range [1, "
					+ "255]");

		this.ttl = ttl;
	}

	/**
	 * @return probe timeout
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout	timeout to be set
	 * @throws IllegalArgumentException if @p timeout is not greater than 0
	 */
	public void setTimeout(int timeout) throws IllegalArgumentException {
		if (timeout <= 0 )
			throw new IllegalArgumentException("Timeout must be positive");
		
		this.timeout = timeout;
	}

	/**
	 * @return number of attempts
	 */
	public int getAttempts() {
		return attempts;
	}

	/**
	 * @param attempts	attempts number to be set
	 * @throws IllegalArgumentException if @p attempts is not greater than 0
	 */
	public void setAttempts(int attempts) throws IllegalArgumentException {
		if (attempts <= 0 )
			throw new IllegalArgumentException("Timeout must be positive");
		
		this.attempts = attempts;
	}

};
