/**
 * This file contains the Interface class definition.
 * This class is used to carry information about an interface
 * discovered by MDA Traceroute, such as its IPv4 address and
 * the list of flows traversing it.
 */
package com.mda.types;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.http.conn.util.InetAddressUtils;

import android.util.Log;
import android.util.SparseArray;

import com.mda.NextHops;
import com.mda.Prober;
import com.mda.exceptions.ProbeException;

/**
 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
 * A discovered interface.
 * Used to carry informations that can be associated with
 * a discovered interface, such as its IPv4 address
 * and flows identifiers (ports) that can be used to reach it.
 */
public class Interface implements Comparable<Interface> {
	private static final String TAG = "Interface";
	
	/**
	 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
	 * Possible types of load balancer we can distinguish.
	 */
	public enum BalanceType {
		NO_BALANCE,
		PER_FLOW_BAL,
		PER_PACKET_BAL,
		UNKNOWN_BAL
	};
	
	/**
	 * @author Fabio Carrara <fabio.blackdragon@gmail.com>
	 * Data structure containing information needed to getFlowPort()
	 * to correctly guess new flow ports.
	 */
	public static class ProbeContext {
		public ProbeParameters params = null;
		public int interfacesInHop;
		
		public ProbeContext() {}
		public ProbeContext(ProbeParameters params, int interfacesInHop) {
			this.params = params;
			this.interfacesInHop = interfacesInHop;
		}
	};
	
	protected InetAddress address;
	protected BalanceType loadBalType = BalanceType.NO_BALANCE; 
	
	// Map containing <hop number, list of ports> entries
	// The ports identify uniquely the flows
	
	protected SparseArray<List<Integer>> hopFlowSet = new SparseArray<List<Integer>>();
	protected Set<Interface> successorsSet = new TreeSet<Interface>();
	protected boolean isUnresponsiveHop = false;
	
	/**
	 *  Create an unresponsive interface
	 */
	
	static public Interface UnresponsiveHopFactory() {
		try {
			Interface i = new Interface(InetAddress.getByName("0.0.0.0"));
			i.isUnresponsiveHop = true;
			return i;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Local Interface. It serves to initialize the algorithm.
	 */
	static public Interface getLocalInterface(boolean useIPv4) {
        try {
			Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
			while (nis.hasMoreElements()) {
				NetworkInterface ni = nis.nextElement();
				Enumeration<InetAddress> addrs = ni.getInetAddresses();
				while (addrs.hasMoreElements()) {
					InetAddress addr = addrs.nextElement();
					if (!addr.isLoopbackAddress() && InetAddressUtils.isIPv4Address(addr.getHostAddress()) == useIPv4)
						return new Interface(addr);
				}
			}
			
			return new Interface(InetAddress.getLocalHost());
		} catch (Exception e) {
			return Interface.UnresponsiveHopFactory();
		}
	}
	
	/**
	 * Construct an interface container
	 * @param address the address of the interface
	 */
	public Interface(InetAddress address) {
		this.address = address;
	}

	/**
	 * @return the address contained as an @t InetAddress
	 */
	public InetAddress getInetAddress() {
		return address;
	}
	
	/**
	 * @return the address contained in a string format
	 */
	public String getStringAddress() {
		if (isUnresponsiveHop)
			return "*";
		
		return address.getHostAddress();
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(InetAddress address) {
		this.address = address;
	}
	
	/**
	 * Set the interface address by dotted-decimal quad or hostname lookup.
	 * @param address the address to set
	 */
	public void setAddressFromString(String address) {
		try {
			this.address = InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			Log.d(TAG, "Lookup of " + address + " failed");
			e.printStackTrace();
		}
	}

	/**
	 * Return a port number that can be used to reach this interface
	 * at hop @p hop.
	 * In case there are more than one port number available, this function
	 * returns one of them.
	 * @param hop hop number
	 * @return a port number, -1 if no ports are available at hop @hop
	 */
	public int getFlowPort(int hop) {
		
		if (hopFlowSet.get(hop) == null) {
			if (!isUnresponsiveHop())
				Log.w(TAG, "No flows at hop " + hop + " to reach " + this.getStringAddress());
			return -1;
		}
		
		// Get the first port of the list of ports associated with the hop
		
		return hopFlowSet.get(hop).get(0);
	}
	
	/**
	 * Return a port number that can be used to reach this interface
	 * at hop @p hop.
	 * In case there are more than one port number available, different
	 * values of @p k should randomize the port number returned.
	 * @param hop hop number
	 * @param k randomizer integer
	 * @param context context of the probe to generate a valid port
	 * @return a port number, -1 if no ports are available at hop @hop
	 */
	public int getFlowPort(int hop, int k, ProbeContext context) {
		
		ProbeResult res;
		
		if (hopFlowSet.get(hop) == null)
			hopFlowSet.append(hop, new LinkedList<Integer>());
		
		if (k < hopFlowSet.get(hop).size())
			return hopFlowSet.get(hop).get(k);
		
		// Try as many times as were needed to discover interfaces
		
		final int attempts = NextHops.probesToSend(context.interfacesInHop);
		
		// Otherwise no other flow ports are available, guess a new one		
		
		for (int i = 0; i < attempts; i++) {
		
			int flowPort;
			
			do { // guess a port not contained in the FlowSet
				flowPort = (int) (Math.random()*(65535 - 1025)) + 1025;
			} while (hopFlowSet.get(hop).contains(flowPort));
			
			// CASE hop == 0:
			//		all ports are good to reach localhost
			// CASE context.interfacesInHop == 1:
			// 		only one interface at this hop,
			// 		all ports are good to reach this interface,
			// 		pick one at random
			if (hop == 0 || context.interfacesInHop == 1) {
				hopFlowSet.get(hop).add(flowPort);
				return flowPort;
			}
			
			context.params.setSourcePort(flowPort);
			context.params.setTtl(hop);

			try {
				res = (new Prober(context.params)).send();
				// if I've reached this interface
				if (res.getInterfaceIp().equals(this.getStringAddress())) {
					hopFlowSet.get(hop).add(flowPort);
					return flowPort;
				}
			} catch (ProbeException e) {}
		}
		Log.w(TAG, "No port found (after " + attempts + " attempts!) to reach " + this.getStringAddress() + " at hop " + hop);
		
		return -1;
	}

	/**
	 * Return all the port numbers that can be used to reach this interface
	 * at hop @p hop.
	 * In case there are more than one port number available, this function
	 * returns one of them.
	 * @param hop hop number
	 * @return a list containing all valid port numbers, null if no ports
	 * are available at hop @hop
	 */
	public List<Integer> getFlowPorts(int hop) {
		
		if (hopFlowSet.get(hop) == null) {
			if (!isUnresponsiveHop())
				Log.w(TAG, "No flows at hop " + hop + " to reach " + this.getStringAddress());
			return null;
		}
		// get the first port of the list of ports associated with the hop
		return hopFlowSet.get(hop);
	}
	
	/**
	 * Add a port with which I can reach this interface at hop @p hop.
	 * @param hop the hop number at which I find this interface
	 * @param port the port number
	 */
	public void setFlowPort(int hop, int port) {
		List<Integer> portList = hopFlowSet.get(hop);
		
		if (portList == null) {
			portList = new LinkedList<Integer>();
			hopFlowSet.append(hop, portList);
		}
		
		portList.add(port);
	}
	
	/**
	 * @return whether this interface is a load balancer
	 */
	public boolean isLoadBalancer() {
		return loadBalType != BalanceType.NO_BALANCE;
	}

	/**
	 * @return whether this interface represents an unresponsive hop
	 */
	public boolean isUnresponsiveHop() {
		return isUnresponsiveHop;
	}
	
	/**
	 * @return an enum representing the type of load balancer
	 * @see BalanceType
	 */
	public BalanceType getLoadBalancingType() {
		return loadBalType;
	}
	
	/**
	 * @param balType One of the possible @t BalanceType to be set
	 * @see BalanceType
	 */
	public void setLoadBalancer(BalanceType balType) {
		this.loadBalType = balType;
	}

	/**
	 * @return the list of successors of this interface
	 */
	public Set<Interface> getSuccessorsSet() {
		return successorsSet;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Interface [address=" + address + ", loadBalType=" + loadBalType
				+ ", hopFlowSet=" + hopFlowSet + ", successorsList="
				+ successorsSet + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Interface other = (Interface) obj;
		if (address == null) {
			if (other.address != null)
				return false;
	
		} else if (!address.equals(other.address))
			return false;

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Interface other) {
		if (other.equals(this))
			return 0;
		
		return address.getHostAddress().compareTo(other.address.getHostAddress());
	}

	/**
	 * Adds an Interface object in the successor set of the current Interface object.
	 * If the Interface to be added is already present, the old instance is returned.
	 * @param s the Interface object to be added to the successors set
	 * @return the old successor instance if @p s is already present, otherwise @p s is returned
	 */
	public Interface addSuccessor(Interface s) {
		Iterator<Interface> it = successorsSet.iterator();
		while (it.hasNext()) {
			Interface p = it.next();
			if (p.equals(s))
				return p;
		}
		successorsSet.add(s);
		return s;
	}
}
