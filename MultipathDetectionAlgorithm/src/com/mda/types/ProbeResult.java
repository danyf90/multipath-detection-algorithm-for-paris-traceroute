package com.mda.types;

import android.util.Patterns;

/**
 * Result of the probe.
 * @param interfaceIp	IP address of the interface which has replied to the probe
 * @param ttl			probe response Time To Live
 * @param rtt			probe Round Trip Time
 * @param code probe response code
 */
public class ProbeResult {
	public final static int DEST_REACHED			= 0;
	public final static int NO_ERROR				= 1;
	public final static int TIMEOUT_OCCURRED		= 2;
	public final static int ICMP_DEST_UNREACH		= 3;
	public final static int UNEXPECTED_ICMP_MSG		= 4;
	public final static int NETWORK_ERROR			= 5;
	public final static int ALREADY_IN_USE_ERROR	= 6;
	public final static int ILLEGAL_ARGUMENT		= 7;
	
	private final static int MAX_CODE_INDEX = 7;
	private final static String[] CODE_STRING = {
		"Destination reached",
		"Destination not reached",
		"Timeout occurred",
		"Unexpected ICMP message",
		"Network error",
		"Port already in use",
		"Illegal argument"};

	protected String	interfaceIp;
	protected int		ttl;
	protected long		rtt;
	protected int		code;

	public ProbeResult(String interfaceIp, int ttl, long rtt, int code) throws IllegalArgumentException {
		setInterfaceIp(interfaceIp);
		setTtl(ttl);
		setRtt(rtt);
		setCode(code);
	}

	/**
	 * @return ip source address of the probe response
	 */
	public String getInterfaceIp() {
		return interfaceIp;
	}

	/**
	 * @param interfaceIp source address to be set
	 * @throws IllegalArgumentException if @p interfaceIp port is neither a valid IP address nor "*"
	 */
	public void setInterfaceIp(String interfaceIp) throws IllegalArgumentException {
		if (interfaceIp == null)
			this.interfaceIp = "*";
		else if (Patterns.IP_ADDRESS.matcher(interfaceIp).matches())
			this.interfaceIp = interfaceIp;
		else
			throw new IllegalArgumentException("Destination IP must be in the"
					+ " form a.b.c.d");

	}

	/**
	 * @return Time To Live field of the probe response
	 */
	public int getTtl() {
		return ttl;
	}

	/**
	 * @param ttl ttl to be set
	 * @throws IllegalArgumentException if (ttl < 1 || ttl > 255)
	 */
	public void setTtl(int ttl) throws IllegalArgumentException {
		if (ttl < 1 || ttl > 255)
			throw new IllegalArgumentException("TTL must be in the range [1, "
					+ "255]");

		this.ttl = ttl;
	}

	/**
	 * @return Round Trip Time (in microseconds) of the probe
	 */
	public long getRtt() {
		return rtt;
	}

	/**
	 * @param rtt rtt (in microseconds) to be set
	 * @throws IllegalArgumentException if (rtt < 0)
	 */
	public void setRtt(long rtt) throws IllegalArgumentException {
		if (rtt < 0)
			throw new IllegalArgumentException("RTT must be positive");

		this.rtt = rtt;
	}

	/**
	 * @return probe response code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @return human readable response code
	 */
	public static String getCodeString(int code) {
		if (code < 0 || code > MAX_CODE_INDEX)
			return null;
		
		return CODE_STRING[code];
	}

	/**
	 * @param code code to be set
	 */
	public void setCode(int code) {
		this.code = code;
	}

};
