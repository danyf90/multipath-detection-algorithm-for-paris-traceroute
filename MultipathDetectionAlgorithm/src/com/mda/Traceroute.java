/**
 * This file contains the class definition of a Java 
 * implementation of the classic traceroute algorithm.
 */
package com.mda;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.os.Handler;
import android.util.Log;

import com.mda.exceptions.ProbeException;
import com.mda.types.Interface;
import com.mda.types.ProbeParameters;
import com.mda.types.ProbeResult;

/**
 * @author andrea
 * 
 */
public class Traceroute extends Thread {
	private static final String TAG					= "Traceroute";
	
	public static final short 	DEFAULT_INIT_TTL	= 1;
	public static final short 	DEFAULT_MAX_TTL		= 255;
	public static final int 	DEFAULT_SOURCE_PORT	= 8080;
	public static final int 	DEFAULT_DEST_PORT	= 80;
	public static final int 	DEFAULT_TIMEOUT_MS	= 5000;
	public static final int 	DEFAULT_ATTEMPTS	= 1;
	public static final int 	DEFAULT_UNRESPONSIVE= 1;

	
	public static final int MSG_START			= 1;
	public static final int MSG_INTERFACE		= 2;
	public static final int MSG_DEST_REACHED	= 3;
	public static final int MSG_END				= 4;
	
	protected InetAddress destinationIP = null;
	protected InetAddress localIP = null;
	protected int sourcePort;
	protected int destinationPort;
	protected short maxTTL;
	protected short initTTL;
	protected int timeout;
	protected int attempts;
	protected int maxUnresponsiveHops;
		
	private Handler handler; 
	
	/**
	 * Constructs a Traceroute instance. 
	 * @param address dotted-decimal ip string or hostname for the destination address
	 * @param h	Handler instance to which send messages containing algorithm outputs
	 */
	public Traceroute(String address, Handler h) {
		setDestinationAddress(address);
		this.sourcePort = DEFAULT_SOURCE_PORT;
		this.destinationPort = DEFAULT_DEST_PORT;
		this.initTTL = DEFAULT_INIT_TTL;
		this.maxTTL = DEFAULT_MAX_TTL;
		this.timeout = DEFAULT_TIMEOUT_MS;
		this.attempts = DEFAULT_ATTEMPTS;
		this.maxUnresponsiveHops = DEFAULT_UNRESPONSIVE;
		this.handler = h; 
	}
	
	/**
	 * Constructs a Traceroute instance.
	 * @param address dotted-decimal ip string or hostname representing the destination address
	 * @param h	Handler instance to which send messages containing algorithm outputs
	 * @param destPort the destination port of the target 
	 * @param initTTL the initial TTL from which the traceroute will start
	 * @param maxTTL the maximum hop number reachable by the algorithm
	 * @param timeout maximum time in milliseconds before giving up waiting for a probe response
	 * @throws IllegalArgumentException when parameters are out of range:
	 * 		destPort > 65535 || destPort < 0
	 * 		initTTL < 1
	 * 		maxTTL < initTTL
	 * 		timeout < 0
	 */
	public Traceroute(String address, Handler h, int sourcePort, int destPort, short initTTL, short maxTTL, int timeout, int attempts, int maxUnresponsiveHops)
		throws IllegalArgumentException {
		this.handler = h;
		setDestinationAddress(address);
		setLocalAddress(Interface.getLocalInterface(true).getStringAddress());
		setSourcePort(sourcePort);
		setDestPort(destPort);
		setInitTTL(initTTL);
		setMaxTTL(maxTTL);
		setTimeout(timeout);
		setAttempts(attempts);
		setMaxUnresponsiveHops(maxUnresponsiveHops);
		
		Log.d(TAG, initTTL + " ");
	}
	
	/**
	 * @return the destination address as an @t InetAddress
	 */

	public InetAddress getDestinationAddress() {
		return this.destinationIP;
	}
	
	/**
	 * @param i the destination address to be set
	 */
	public void setDestinationAddress(InetAddress i) {
		this.destinationIP = i;
	}
	
	/**
	 * @param i dotted-decimal ip string or hostname of the destination address to be set
	 */
	public void setDestinationAddress(String i) {
		try {
			this.destinationIP = InetAddress.getByName(i);
		} catch (UnknownHostException e) {
			Log.w(TAG, "Lookup of " + i + " failed");
			e.printStackTrace();
		}
	}
	
	/**
	 * @param i the local address to be set
	 */
	public void setLocalAddress(InetAddress i) {
		this.localIP = i;
	}
	
	/**
	 * @param i dotted-decimal ip string or hostname of the local address to be set
	 */
	public void setLocalAddress(String i) {
		try {
			this.localIP = InetAddress.getByName(i);
		} catch (UnknownHostException e) {
			Log.w(TAG, "Lookup of " + i + " failed");
			e.printStackTrace();
		}
	}

	/**
	 * @return the destination port in use
	 */
	public int getSourcePort() {
		return this.sourcePort;
	}
	
	/**
	 * @param port the destination port to be set
	 * @throws IllegalArgumentException if (port > 65535 || port < 0)
	 */
	public void setSourcePort(int port) throws IllegalArgumentException {
		if (port < 1024 || port > 65535)
			throw new IllegalArgumentException("Port number out of range");
	
		this.sourcePort = port;
	}

	/**
	 * @return the destination port in use
	 */
	public int getDestPort() {
		return this.destinationPort;
	}
	
	/**
	 * @param port the destination port to be set
	 * @throws IllegalArgumentException if (port > 65535 || port < 0)
	 */
	public void setDestPort(int port) throws IllegalArgumentException {
		if (port > 65535 || port < 0)
			throw new IllegalArgumentException("Port number out of range");
	
		this.destinationPort = port;
	}
	
	/**
	 * @return the minimum hop number in use
	 */
	public short getInitTTL() {
		return this.initTTL;
	}
	
	
	public void setInitTTL(short initTTL) throws IllegalArgumentException {
		if (initTTL < 1)
			throw new IllegalArgumentException("Minimum hop number cannot be less than one");
			
		this.initTTL = initTTL;
	}
	
	/**
	 * @return the maximum hop number in use
	 */
	public short getMaxTTL() {
		return this.maxTTL;
	}
	
	
	public void setMaxTTL(short maxTTL) throws IllegalArgumentException {
		if (maxTTL < initTTL)
			throw new IllegalArgumentException("Maximum TTL cannot be less than the minimum one");
			
		this.maxTTL = maxTTL;
	}

	/**
	 * @return get the current timeout for probe response waiting
	 */
	public int getTimeout() {
		return this.timeout;
	}
	
	/**
	 * @param timeout the timeout to be set for waiting for probe responses
	 * @throws IllegalArgumentException if (timeout < 0)
	 */
	public void setTimeout(int timeout) throws IllegalArgumentException {
		if (timeout < 0)
			throw new IllegalArgumentException("Timeout value must be positive");
			
		this.timeout = timeout;
	}

	/**
	 * @return get the current number of attempts for probe
	 */
	public int getAttempts() {
		return this.attempts;
	}
	
	/**
	 * @param attempts attempts number to be set
	 * @throws IllegalArgumentException if (attempts < 1)
	 */
	public void setAttempts(int attempts) throws IllegalArgumentException {
		if (attempts < 1)
			throw new IllegalArgumentException("Timeout value must be greater than 0");
			
		this.attempts= attempts;
	}

	/**
	 * @return get the current number of unresponsive hops before stop
	 */
	public int getMaxUnresponsiveHops() {
		return this.maxUnresponsiveHops;
	}

	/**
	 * @param maxUnresponsiveHops the maximum number of consecutive unresponsive hops
	 * @throws IllegalArgumentException if (maxUnresponsiveHops < 1)
	 */
	public void setMaxUnresponsiveHops(int maxUnresponsiveHops) throws IllegalArgumentException {
		if (maxUnresponsiveHops < 1)
			throw new IllegalArgumentException("MaxUnresponsiveHops value must be greater than 0");

		this.maxUnresponsiveHops = maxUnresponsiveHops;
	}

	@Override
	public void run() {
		int h = 0;
		int unresponsiveHopsCount = 0;
		output(MSG_START);
		
		// First hop. Probing local IP address
		ProbeResult r = sendProbe(true, 1);
		if (r.getCode() == ProbeResult.DEST_REACHED)
			r.setCode(ProbeResult.NO_ERROR);
		
		output(MSG_INTERFACE, r);
		log(0, r);
		
		// Probing for next hops
hoploop:for (h = initTTL; h <= maxTTL; h++) {
			r = sendProbe(false, h);
			// output the result
			output(MSG_INTERFACE, r);
			
			log(h, r);
			
			switch(r.getCode()){
			case ProbeResult.NO_ERROR:
				unresponsiveHopsCount = 0;
				break;
			case ProbeResult.DEST_REACHED:
				output(MSG_DEST_REACHED);
				return;
			default:
				unresponsiveHopsCount++;
				if (unresponsiveHopsCount == maxUnresponsiveHops) {
					Log.d(TAG, unresponsiveHopsCount + " consecutive unresponsive hops");
					break hoploop;
				}
			}

			if (this.isInterrupted()) {
				Log.v(TAG, "Algorithm is interrupted.");
				return;
			}
		}
		
		// Traceroute ended without reaching the destination
		output(MSG_END);
		
		Log.d(TAG, "Traceroute Ended");
		
	}

	/**
	 * Makes output of the traceroute available as message sent
	 * to the Handler instance specified in the constructor.
	 * @see Traceroute()
	 * @param r the ProbeResult
	 */
	private void output(int what, Object obj) {
		handler.obtainMessage(what, obj).sendToTarget();
	}
	private void output(int what) {
		handler.obtainMessage(what, null).sendToTarget();
	}
	
	private void log(int h, ProbeResult r) {
		if (r.getCode() == ProbeResult.NO_ERROR || r.getCode() == ProbeResult.DEST_REACHED)
			Log.d(TAG, String.format("%02d", h) + ": " + r.getInterfaceIp());
		else
			Log.d(TAG, String.format("%02d", h) + ": " + ProbeResult.getCodeString(r.getCode()));
		
	}

	/**
	 * Sends a traceroute probe
	 * @param localhost			true for local destination, false remote destination
	 * @param ttl				probe TTL
	 * @param sourcePort			probe source port
	 * @return ProbeResult
	 */
	private ProbeResult sendProbe(boolean localhost, int ttl) {
		Prober		p;
		ProbeResult r  = null;

		// Create a probe
		if (localhost) // Localhost destination. 
			p = new Prober(new ProbeParameters(localIP.getHostAddress(), sourcePort,
					destinationPort, ttl, timeout, attempts));
		else // Remote destination
			p = new Prober(new ProbeParameters(destinationIP.getHostAddress(), sourcePort,
					destinationPort, ttl, timeout, attempts));
		
		// Send the probe and get the result or retransmit the packet
		for (int i = 0; i < attempts; i++) {
			try {
				r = p.send();
				r.setTtl(ttl);
				return r;
			} catch (ProbeException e) {
				r = e.getProbeResult();
				continue;
			}
		}
		
		r.setTtl(ttl);
		return r;
	}
}
