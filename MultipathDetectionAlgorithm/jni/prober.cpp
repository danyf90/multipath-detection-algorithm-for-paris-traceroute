/**
 * This file contains the C++ implementation of an UDP prober.
 * It sends an UDP packet with the specified address and port, and TTL.
 * Then it waits for an ICMP error message (TIME_EXCEEDED for intermediate
 * nodes or DEST_UNREACH for the target node.
 */
#include <arpa/inet.h>
#include <errno.h>
#include <linux/errqueue.h>
#include <linux/icmp.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#include "prober.h"

#define BUFFSIZE 1024
#define MAXEVENTS 10

#define debug

#ifdef debug

#include <android/log.h>
#define LOG(format, ...) __android_log_print(ANDROID_LOG_DEBUG, __FILE__, format , ##__VA_ARGS__);

#else

#define LOG(...)

#endif

/**
 *
 */
struct ProbeParameters {
	jstring	destIp;
	jint	sourcePort;
	jint	destPort;
	jint	ttl;
	jint	timeout;
};

struct ProbeResult {
	jstring	interfaceIp;
	jint	ttl;
	jlong	rtt;
	jint	code;
};

ProbeParameters* getParameters(JNIEnv*, jobject);
jobject result(JNIEnv*, ProbeResult);
jstring getSourceAddress(JNIEnv*, sock_extended_err*);
jint getTtl(void* ttl);
jlong timediff(timeval, timeval);
jint getJavaIntConst(JNIEnv*, const char*, const char*);

/**
 * Sends an UDP probe and waits for an ICMP error message.
 *
 * @param parameters	probe parameters
 *
 * @return	probe result
 */
extern "C" JNIEXPORT jobject JNICALL Java_com_mda_Prober_sendProbe(JNIEnv* env, jobject _this, jobject parameters) {

	timeval				sendTime, recvTime;
	ProbeParameters 	*p;
	ProbeResult			r;

	// sockets
	int					s, y = 1, res, probePayload = 1;
	sockaddr_in			source, dest;
	pollfd				fds;


	// handle error message
	char				buffer[BUFFSIZE], control[BUFFSIZE];
	cmsghdr				*cmsg;
	iovec				iov;
	msghdr				h;
	sock_extended_err	*socket_error;

	p = getParameters(env, parameters);

	if (p == NULL) {
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "ILLEGAL_ARGUMENT");
		goto error;
	}

	// create UDP socket

	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		LOG("socket: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// set source port

	if (p->sourcePort != 0) {
		memset(&source,0,sizeof(source));
		source.sin_family = AF_INET;
		source.sin_port = p->sourcePort;
		source.sin_addr.s_addr = INADDR_ANY;
		if (bind(s, (struct sockaddr *)&source, sizeof(source)) == -1) {
			LOG("hop %d: bind(%i): %s", p->ttl, p->sourcePort, strerror(errno));
			if (errno == EADDRINUSE)
				r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "ALREADY_IN_USE_ERROR");
			else
				r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
			goto error;
		}
	}
	// set destination port and ip

	memset(&dest, 0, sizeof(dest));
	dest.sin_family = AF_INET;
	dest.sin_port = p->destPort;
	inet_pton(AF_INET, env->GetStringUTFChars(p->destIp, NULL), &dest.sin_addr);
	if (connect(s, (sockaddr*)&dest, sizeof(dest)) == -1) {
		LOG("connect: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// set ttl

	if (setsockopt(s, IPPROTO_IP, IP_TTL, &p->ttl, sizeof(p->ttl)) == -1)  {
		LOG("setttl: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// set flag to receive ip errors

	if (setsockopt(s, SOL_IP, IP_RECVERR, &y, sizeof(y)) == -1)  {
		LOG("setiprecverr: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// set flag to receive ttl

	if (setsockopt(s, SOL_IP, IP_RECVTTL, &y, sizeof(y)) == -1)  {
		LOG("setiprecverr: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// send probe

	gettimeofday(&sendTime, NULL);
	if (send(s, &probePayload, sizeof(probePayload), 0) == -1) {
		LOG("send: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	// initialize iovec and header of response message

	iov.iov_base = &buffer;
	iov.iov_len = sizeof(buffer);

	h.msg_name = NULL;
	h.msg_namelen = 0;
	h.msg_iov = &iov;
	h.msg_iovlen = 1;
	h.msg_flags = 0;
	h.msg_control = &control;
	h.msg_controllen = sizeof(control);

	// suspend till data available or timeout

	memset(&fds, 0 , sizeof(fds));
	fds.fd = s;
	fds.events = POLLMSG;

	// wait for an error

	if ((res = poll(&fds, 1, (long int)p->timeout)) < 0) {
		LOG("poll: %s", strerror(errno));
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
		goto error;
	}

	if (res == 0) { // timeout occured
		r.interfaceIp = NULL;
		r.ttl = 1;
		r.rtt = 0;
		r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "TIMEOUT_OCCURRED");
	}
	else {
		gettimeofday(&recvTime, NULL);
		r.rtt = timediff(recvTime, sendTime);

		if (recvmsg(s, &h, MSG_ERRQUEUE) == -1) {
			LOG("recvmsg: %s", strerror(errno));
			r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NETWORK_ERROR");
			goto error;
		}

		// manage the error message

		for (cmsg = CMSG_FIRSTHDR(&h); cmsg != NULL; cmsg = CMSG_NXTHDR(&h, cmsg)) {
			if (cmsg->cmsg_level == SOL_IP) {
				if (cmsg->cmsg_type == IP_RECVERR) {
					socket_error = (struct sock_extended_err*)CMSG_DATA(cmsg);
					if (socket_error->ee_origin == SO_EE_ORIGIN_ICMP) {
						switch (socket_error->ee_type) {
							case ICMP_TIME_EXCEEDED:
								if (socket_error->ee_code == ICMP_EXC_TTL) {
									r.interfaceIp = getSourceAddress(env, socket_error);
									r.ttl = 1;
									r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "NO_ERROR");
								}
								else {
									LOG("ICMP_TIME_EXCEEDED ERROR, code: %i", socket_error->ee_code);
									r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
									goto error;
								}
								break;
							case ICMP_DEST_UNREACH:
								if (socket_error->ee_code == ICMP_PORT_UNREACH) {
									r.interfaceIp = getSourceAddress(env, socket_error);
									r.ttl = 1;
									r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "DEST_REACHED");
								}
								else {
									LOG("ICMP_DEST_UNREACH ERROR, code: %i", (int) socket_error->ee_code);
									r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
									goto error;
								}
								break;
							default:
								LOG("ICMP_TYPE ERROR , type: %i", (int) socket_error->ee_code);
								r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
								goto error;
						}
					}
					else {
						LOG("UNEXPECTED MESSAGE ORIGIN: %i", (int) socket_error->ee_origin);
						r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
						goto error;
					}
				}
				else if (cmsg->cmsg_type == IP_TTL) {
					r.ttl = getTtl(CMSG_DATA(cmsg));
				}
				else {
					LOG("UNEXPECTED MESSAGE TYPE: %i", (int) cmsg->cmsg_type);
					r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
					goto error;
				}
			}
			else {
				LOG("UNEXPECTED MESSAGE LEVEL: %i", (int) cmsg->cmsg_level);
				r.code = getJavaIntConst(env, "com/mda/types/ProbeResult", "UNEXPECTED_ICMP_MSG");
				goto error;
			}
		}
	}

	close(s);
	return result(env, r);

error:
	r.interfaceIp = NULL;
	r.ttl = 1;
	r.rtt = 0;
	close(s);
	return result(env, r);
}


/**
 * Convert parameters from Java object to C++ structure
 * @param env	jni environment
 * @param r		probe parameters Java object
 * @return	probe parameters C++ structure
 */
ProbeParameters* getParameters(JNIEnv* env, jobject parameters) {
	ProbeParameters *p = new ProbeParameters();
	jclass jProbeParameters;
	jmethodID id;

	jProbeParameters = env->FindClass("com/mda/types/ProbeParameters");

	id = env->GetMethodID(jProbeParameters, "getDestIp", "()Ljava/lang/String;");
	p->destIp = (jstring) env->CallObjectMethod(parameters, id);

	id = env->GetMethodID(jProbeParameters, "getSourcePort", "()I");
	p->sourcePort = env->CallIntMethod(parameters, id);

	id = env->GetMethodID(jProbeParameters, "getDestPort", "()I");
	p->destPort = env->CallIntMethod(parameters, id);

	id = env->GetMethodID(jProbeParameters, "getTtl", "()I");
	p->ttl = env->CallIntMethod(parameters, id);

	id = env->GetMethodID(jProbeParameters, "getTimeout", "()I");
	p->timeout = env->CallIntMethod(parameters, id);

	return p;
};

/**
 * Convert probe result from C++ structure to Java object.
 * @param env	jni environment
 * @param r		probe result C++ structure
 * @return	probe result Java object
 */
jobject result(JNIEnv* env, ProbeResult r) {
	jclass jProbeResult;
	jmethodID id;

	// create a ProbeResult instance

	jProbeResult = env->FindClass("com/mda/types/ProbeResult");
	id = env->GetMethodID(jProbeResult, "<init>", "(Ljava/lang/String;IJI)V");

	return env->NewObject(jProbeResult, id, r.interfaceIp, r.ttl, r.rtt, r.code);
}

/**
 * Returns the source address of the error message.
 * @param env			jni environment
 * @param socket_error	socket error structure
 * @return ip source address of the error message
 */
jstring getSourceAddress(JNIEnv* env, sock_extended_err* socket_error) {
	char ip[16];
	const sockaddr_in *interfaceAddr;

	interfaceAddr = (const sockaddr_in*) SO_EE_OFFENDER(socket_error);
	inet_ntop(AF_INET, &interfaceAddr->sin_addr, ip, 16);
	return env->NewStringUTF(ip);
}

/**
 * Returns the Time to Live of the error message.
 * @param socket_error	socket error structure
 * @return ttl of the error message
 */
jint getTtl(void* data) {
	return *((char*) data);
}

/**
 * Returns the time difference (in microseconds) between two timeval structures
 * @param t1	finish time
 * @param t0	start time
 * @return microseconds elapsed since t0 to t1
 */
jlong timediff(timeval t1, timeval t0) {
	return (t1.tv_sec*1000000 + t1.tv_usec) - (t0.tv_sec*1000000 + t0.tv_usec);
}

/**
 * Returns an int static field defined in a Java class
 * @param env		jni environment
 * @param className	class name
 * @param constName	field name
 * @return field value
 */
jint getJavaIntConst(JNIEnv* env, const char* className, const char* constName) {
	jclass clazz;
	jfieldID id;

	clazz = env->FindClass(className);
	id = env->GetStaticFieldID(clazz, constName, "I");

	return env->GetStaticIntField(clazz, id);
}


