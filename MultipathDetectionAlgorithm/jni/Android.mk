LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_LDLIBS := -llog
LOCAL_MODULE    := prober
LOCAL_SRC_FILES := prober.cpp
include $(BUILD_SHARED_LIBRARY)
APP_PLATFORM := android-8